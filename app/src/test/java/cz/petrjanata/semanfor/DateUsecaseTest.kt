package cz.petrjanata.semanfor

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.api.ApiConstants
import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.usecase.DateUsecase
import org.junit.Assert
import org.junit.Test
import java.util.*

class DateUsecaseTest : BaseTest {

    private val usecase: DateUsecase = App.kodein.instance()
    private val format = ApiConstants.dateFormat
    private val ninthOfNovember = Date(1541757600000L)
    private val expectedMorning = "2018-11-09T08:00:00.000+0100"
    private val expectedEvening = "2018-11-09T18:00:00.000+0100"


    @Test
    fun nevim() {
        val leDate: Date = format.parse("2018-11-09T11:00:00.000+0100")
        SLog.v("leDate: ${leDate.time}")
    }

    @Test
    fun testMorning() {
        val morning = usecase.getMorning(ninthOfNovember)
        val morningString: String = format.format(morning)

        SLog.v(expectedMorning)
        SLog.v(morningString)
        Assert.assertEquals(expectedMorning, morningString)
    }

    @Test
    fun testEvening() {
        val evening = usecase.getEvening(ninthOfNovember)
        val eveningString: String = format.format(evening)

        SLog.v(expectedEvening)
        SLog.v(eveningString)
        Assert.assertEquals(expectedEvening, eveningString)
    }
}