package cz.petrjanata.semanfor

import cz.petrjanata.semanfor.helper.log.SLog
import org.junit.Before

interface BaseTest {

    @Before
    fun beforeTest() {
        SLog.setScope(SLog.Scope.TEST)
    }

}