package cz.petrjanata.semanfor.config

abstract class BaseConfigConstants {
    val BUILD_DEBUG = false
    val SLEEP_AFTER_WORKING_HOURS = false
    val EVENING = 18
}