package cz.petrjanata.semanfor.service.web.devtest

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by Petr Janata on 13.08.2018.
 * For development and testing
 */
interface DevTestRetrofitProtocol {

    @POST("user")
    fun postUserInfo(@Body tokensWrapper: TokensWrapperADO) : Call<Void>

}