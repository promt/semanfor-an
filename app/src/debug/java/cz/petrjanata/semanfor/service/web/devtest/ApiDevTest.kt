package cz.petrjanata.semanfor.service.web.devtest

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.model.ConnectionException
import cz.petrjanata.semanfor.api.ApiConstants
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.service.web.WebServiceHelper
import cz.petrjanata.semanfor.service.web.retrofit.OkHttpClientHelper
import cz.petrjanata.semanfor.service.web.retrofit.RetrofitConfiguration

/**
 * Created by Petr Janata on 13.08.2018.
 */
class ApiDevTest(herokuBaseUrl: String? = null) {

    private val semanforApiService: WebServiceHelper = App.kodein.instance()

    ///instance attributes
    private val baseUrl: String = herokuBaseUrl ?: "http://192.168.1.8:5000/" ?: "https://immense-everglades-39500.herokuapp.com"
    private val headers: Map<String, String> = mapOf(ApiConstants.CONTENT_TYPE to ApiConstants.APPLICATION_JSON)
    private val okClientProvider: OkHttpClientHelper = OkHttpClientHelper()
    private val retrofitProtocol: DevTestRetrofitProtocol = RetrofitConfiguration().getApiCaller(baseUrl, headers, DevTestRetrofitProtocol::class.java, okClientProvider)

    fun postUserInfo(tokensWrapper: TokensWrapperADO): IValueWrapper<Void, ConnectionException> {
        val apiCall = retrofitProtocol.postUserInfo(tokensWrapper)
        return semanforApiService.executeApiCall(apiCall)
    }

}