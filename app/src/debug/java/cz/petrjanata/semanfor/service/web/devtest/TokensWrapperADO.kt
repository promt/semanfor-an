package cz.petrjanata.semanfor.service.web.devtest

/**
 * Created by Petr Janata on 13.08.2018.
 */
class TokensWrapperADO() {

    constructor(serverAuthCode: String): this() {
        this.serverAuthCode = serverAuthCode
    }

    constructor(serverAuthCode: String, pushToken: String): this(serverAuthCode) {
        this.pushToken = pushToken
    }

    lateinit var serverAuthCode: String
    lateinit var pushToken: String

}
