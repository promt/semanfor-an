package cz.petrjanata.semanfor.config

abstract class BaseConfigConstants {
    val BUILD_DEBUG = true
    val SLEEP_AFTER_WORKING_HOURS = true
    val EVENING = 23
}