package cz.petrjanata.semanfor.view

import cz.petrjanata.semanfor.api.model.DriverADO
import cz.petrjanata.semanfor.api.model.DriverInfoADO
import cz.petrjanata.semanfor.api.model.LotState
import cz.petrjanata.semanfor.api.model.PlaceADO

object Mock {

    val defaultDriverInfo: DriverInfoADO
        get() = ondrovaRezervace

    private val testCalendarID: String = "eman.cz_31353334323632353536@resource.calendar.google.com"
    private val ondra: DriverADO = DriverADO("Miroslav Kalousek", "miroslav.kalousek@eman.cz", "+420 722 000 111", "Škoda Octavia červená", "MKH0011")


    val freeDriverInfo: DriverInfoADO = DriverInfoADO(
            LotState.FREE,
            setOf(
                    PlaceADO("01", "Place1", testCalendarID, null),
                    PlaceADO("02", "Place2", testCalendarID, null),
                    PlaceADO("03", "Place3", testCalendarID, null),
                    PlaceADO("04", "Place4", testCalendarID, null),
                    PlaceADO("05", "Place5", testCalendarID, null),
                    PlaceADO("06", "Place6", testCalendarID, null),
                    PlaceADO("07", "Place7", testCalendarID, null),
                    PlaceADO("08", "Place8", testCalendarID, null),
                    PlaceADO("09", "Place9", testCalendarID, null),
                    PlaceADO("10","Place10", testCalendarID, null),
                    PlaceADO("11","Place11", testCalendarID, null),
                    PlaceADO("12","Place12", testCalendarID, null),
                    PlaceADO("13","Place13", testCalendarID, null),
                    PlaceADO("14","Place14", testCalendarID, null),
                    PlaceADO("15","Place15", testCalendarID, null)
            ),
            emptySet(),
            null
    )

    val ondrovaRezervace: DriverInfoADO = DriverInfoADO(
            LotState.FREE,
            setOf(
                    PlaceADO("02", "Place2", testCalendarID, null),
                    PlaceADO("03", "Place3", testCalendarID, null)
            ),
            setOf(
                    PlaceADO("01", "Place1", testCalendarID, ondra)
            ),
            null
    )
}