package cz.petrjanata.semanfor.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.location.LocationManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.github.salomonbrys.kodein.instance
import com.google.firebase.iid.FirebaseInstanceId
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.R
import cz.petrjanata.semanfor.config.ConfigConstants
import cz.petrjanata.semanfor.helper.DebugHelper
import cz.petrjanata.semanfor.helper.executeAsync
import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.model.*
import cz.petrjanata.semanfor.service.AndroidHelper
import cz.petrjanata.semanfor.service.google.SignInHelper
import cz.petrjanata.semanfor.service.receiver.LocationBroadcastReceiver
import cz.petrjanata.semanfor.service.web.WebServiceHelper
import cz.petrjanata.semanfor.service.web.devtest.ApiDevTest
import cz.petrjanata.semanfor.service.web.devtest.TokensWrapperADO
import cz.petrjanata.semanfor.usecase.GetServerAuthCodeUseCase
import cz.petrjanata.semanfor.view.signin.SignInActivity
import kotlin.math.roundToInt
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.support.v7.widget.AppCompatTextView
import cz.petrjanata.semanfor.config.AppConstants
import cz.petrjanata.semanfor.view.reservation.ReservationActivity
import java.util.*


/**
 * Created by Petr Janata on 10.04.2018.
 */
class TestingActivity : AppCompatActivity {

    companion object {
        private var instanceCount: Int = 0
        public var herokuBaseUrl: String = "http://192.168.1.5:5000"
//            get() = null
        private set
    }

    private val TAG = javaClass.simpleName

    private val androidHelper: AndroidHelper = App.kodein.instance()
    private val signInHelper: SignInHelper = App.kodein.instance()
    private val getServerAuthCodeUseCase: GetServerAuthCodeUseCase = App.kodein.instance()
    private val semanforApiService: WebServiceHelper = App.kodein.instance()
    private val configConstants: ConfigConstants = App.kodein.instance()


    private val activity = this
    private val notificationDelegate = TestingNotificationDelegate(this)

    private val textView: TextView by lazy { findViewById<TextView>(R.id.textView) }
    private val herokuBaseUrlEt: EditText by lazy { findViewById<EditText>(R.id.herokuIpEt) }
    private val simpleImageView: ImageView by lazy { findViewById<ImageView>(R.id.simpleImageView) }
    private val circleTextView: AppCompatTextView by lazy { findViewById<AppCompatTextView>(R.id.circleTextView) }

    constructor() : super() {
        instanceCount++
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_testing)
        if (instanceCount == 1) {
            openApp(null)
        }
        herokuBaseUrlEt.setText(herokuBaseUrl)
    }

    override fun onPause() {
        super.onPause()
        herokuBaseUrl = herokuBaseUrlEt.text.toString()
    }

    fun openApp(viewButton: View?) {
        startActivity(SignInActivity::class.java)
        DebugHelper.postIn(5000L) { startActivity(SettingsActivity::class.java) }
    }

    fun mainAction(viewButton: View?) {
        showMainNotification(viewButton)
    }

    fun postUserInfo(buttonView: View) {
        val baseUrl: String = herokuBaseUrlEt.text.toString()
        getServerAuthCodeUseCase.getServerAuthCodeSilentlyAsync { serverAuthCode ->
            serverAuthCode ?: return@getServerAuthCodeSilentlyAsync DebugHelper.showShortToast("serverAuthCode is null")
            executeAsync {
                val pushToken: String = FirebaseInstanceId.getInstance().token ?: "null"
                val userADO = TokensWrapperADO(serverAuthCode, pushToken)
                val response: IValueWrapper<Void, ConnectionException> = ApiDevTest(baseUrl).postUserInfo(userADO)
                val message: String
                val ex = response.exception
                if (ex == null) {
                    message = "serverAuthCode $serverAuthCode uploaded successfully"
                } else {
                    message = "When uploading serverAuthCode an error occurred. ${ex.message ?: ex}"
                }

                DebugHelper.showShortToast(message)
                SLog.v(TAG, message)
            }
        }
    }

    fun hasLocationPermission(viewButton: View?) {
        val hasPermission: Boolean = androidHelper.isLocationPermissionGranted(this)
        textView.text = "hasPermission: $hasPermission"
    }

    fun showToken(buttonView: View) {
    }

    fun silentSignIn(buttonView: View) {
        signInHelper.silentSignIn{textView.text = "account: $it"}
    }

    fun getFirebaseToken(buttonView: View) {
        val token = FirebaseInstanceId.getInstance().getToken()
        SLog.v("firebase token", token)
        textView.text = "firebase token: $token"
    }

    @SuppressLint("MissingPermission")
    fun registerToLocation(buttonView: View?) {
        if (androidHelper.isLocationPermissionGranted(this)) {

            val locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            locationManager.addProximityAlert(
                    configConstants.GPS_PARKING_LATITUDE,
                    configConstants.GPS_PARKING_LONGITUDE,
                    configConstants.GPS_PARKING_RADIUS,
                    -1,
                    LocationBroadcastReceiver.getRadiusEnterPIntent(this)
            )
        } else {
            startActivityForResult(PermissionActivity.getIntent(this, Manifest.permission.ACCESS_FINE_LOCATION), 42)
        }
    }

    fun showMainNotification(viewButton: View? = null) {
        notificationDelegate.showMainNotification(viewButton)
    }

    fun createBitmap(viewButton: View? = null) {
        var theBitmap: Bitmap = BitmapFactory.decodeResource(resources, R.drawable.ic_notification_big)
        val textSize: Float = resources.getDimension(R.dimen.notification_place_number_text_size)
        theBitmap = textAsBitmap("17", textSize, Color.WHITE)


        simpleImageView.setImageBitmap(theBitmap)
    }

    fun textAsBitmap(text: String, textSize: Float, textColor: Int): Bitmap {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.textSize = textSize
        paint.color = textColor
        paint.textAlign = Paint.Align.CENTER
        var baseline = -paint.ascent() // ascent() is negative
        val width: Int    // round
        width = (paint.measureText(text) + 0.5f).roundToInt()
        val height: Int
        height = (baseline + paint.descent() + 0.5f).roundToInt()

        var image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val drawable: Drawable = resources.getDrawable(R.drawable.green_circle)
        image = drawableToBitmap(drawable)
        baseline = (image.height/1F) - baseline

        baseline = (image.height - height) / 2F
        baseline = textSize + baseline

        val xCoordinate: Float = image.width / 2F

        val canvas = Canvas(image)
        canvas.drawText(text, xCoordinate, baseline, paint)
        return image
    }

    fun drawableToBitmap(drawable: Drawable): Bitmap {

        if (drawable is BitmapDrawable) {
            return drawable.bitmap
        }

        val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)

        return bitmap
    }

    fun openReservationActivity(buttonView: View? = null) {
        val day: Long = 1000*60*60*24
        val tomorrow: Date = Date(Date().time + day)
        val intent = Intent(this, ReservationActivity::class.java)
        intent.putExtra(AppConstants.RESERVATION_DATE_EXTRA_KEY, tomorrow)
        startActivity(intent)
    }

    private fun startActivity(activityClass: Class<out Activity>): Unit {
        startActivity(Intent(this, activityClass))
    }

    private fun startActivitites(classes: Array<Class<out Activity>>): Unit {
        val intent = Intent()
        startActivity(intent)
    }
}