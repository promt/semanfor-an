package cz.petrjanata.semanfor.view

import android.view.View
import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.api.model.DriverInfoADO
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.model.ResponseValidationException
import cz.petrjanata.semanfor.model.SemanforException
import cz.petrjanata.semanfor.model.ValueWrapper
import cz.petrjanata.semanfor.view.notification.NotificationView

class TestingNotificationDelegate(val activity: TestingActivity) {

    private val notificationView: NotificationView = App.kodein.instance()

    fun showMainNotification(viewButton: View? = null) {
        showHappyNotification()
    }

    private fun showHappyNotification() {
        postDriverInfoWrapper(ValueWrapper(Mock.freeDriverInfo))
    }

    private fun showErrorNotification() {
        val ex = ResponseValidationException("Exception from TestingActivity.")
        val wrapper: IValueWrapper<DriverInfoADO, SemanforException> = ValueWrapper<DriverInfoADO, SemanforException>(ex)
        postDriverInfoWrapper(wrapper)
    }

    private fun postDriverInfoWrapper(wrapper: IValueWrapper<DriverInfoADO, SemanforException>) {
        notificationView.showArrivingNotification(wrapper)
    }
}