package cz.petrjanata.semanfor.helper

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.helper.log.SLog
import java.util.concurrent.Executor

/**
 * Class used for debbuging purposes only.
 * It allows you some simple shortcuts.
 */
object DebugHelper {

    private val DEBUG: Boolean = true
    private val context: Context by lazy { App.instance }
    private val log by lazy { SLog }
    private val TAG = "DebugHelper"

    fun showToast(message: Int) {
        showToast(context, message)
    }

    fun showToast(message: String) {
        showToast(context, message)
    }

    fun showToast(context: Context, message: String) {
        uiExecutor.execute { Toast.makeText(context, message, Toast.LENGTH_LONG).show() }
    }

    fun showToast(context: Context, message: Int) {
        uiExecutor.execute { Toast.makeText(context, message, Toast.LENGTH_LONG).show() }
    }

    fun notImplementedYet() {
        val methodName = Exception().fillInStackTrace().stackTrace[1].methodName
        return notImplementedYet(methodName)
    }

    fun notImplementedYet(methodName: String) {
        showToast("Method not implemented yet.\n$methodName")
        SLog.notImplementedYet(methodName)
    }

    fun log() {
        val method = Exception().fillInStackTrace().stackTrace[1].toString()
        log.d("$TAG: method $method called")
    }

    fun log(tag: String) {
        val method = Exception().fillInStackTrace().stackTrace[1].toString()
        log.d("$tag: method $method called")
    }

    private val uiExecutor: Executor = Executor { command -> Handler(Looper.getMainLooper()).post(command) }

    fun sleep(millis: Long) {
        try {
            Thread.sleep(millis)
        } catch (e: InterruptedException) {
            Thread.interrupted()
        }
    }

    fun postInSecond(task: ()->Unit) {
        postIn(1000L, task)
    }

    fun postIn(delay: Long, task: () -> Unit) {
        Thread({
            sleep(delay)
            uiExecutor.execute { task.invoke() }
        }).start()
    }

    fun showShortToast(message: String) {
        uiExecutor.execute { Toast.makeText(context, message, Toast.LENGTH_SHORT).show() }
    }

    fun ifDebug(action: () -> Unit) {
        if (DEBUG) {
            action.invoke()
        }
    }
}