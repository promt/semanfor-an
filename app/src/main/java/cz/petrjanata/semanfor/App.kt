package cz.petrjanata.semanfor

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.github.salomonbrys.kodein.Kodein
import cz.petrjanata.semanfor.helper.inject.SemanforKodeinHelper

/**
 * Created by Petr Janata on 29.03.2018.
 */
class App : Application() {

    companion object {
        @JvmStatic
        val kodein: Kodein by lazy { SemanforKodeinHelper().provideKodein() }

        private lateinit var _instance: App
        @JvmStatic
        val instance: App
            get() = _instance
    }

    val appPreferences: SharedPreferences by lazy {
        getSharedPreferences("appSharedPreferences", Context.MODE_PRIVATE)
    }

    override fun onCreate() {
        super.onCreate()
        _instance = this
    }

}