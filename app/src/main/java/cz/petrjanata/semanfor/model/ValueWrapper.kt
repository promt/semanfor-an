package cz.petrjanata.semanfor.model

class ValueWrapper<V, E : SemanforException> : IValueWrapper<V, E> {

    override var value: V?
    override var exception: E?

    constructor(exception: E?) {
        this.value = null
        this.exception = exception
    }

    constructor( value: V? = null, exception: E? = null) {
        this.value = value
        this.exception = exception
    }


}