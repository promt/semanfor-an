package cz.petrjanata.semanfor.model

object HttpStatus {
    const val OK = 200
    const val CREATED = 201
    const val ACCEPTED = 202
    const val NO_CONTENT = 204
    const val MULTIPLE_CHOICES = 300
    const val NOT_MODIFIED = 304
}