package cz.petrjanata.semanfor.helper.log

import android.util.Log

/**
 * Created by Petr Janata on 29.03.2018.
 */
internal class AndroidInfoLogDelegate : ASemanforLogDelegate() {
    override fun isLoggable(s: String?, i: Int): Boolean = true

    override fun println(priority: Int, tag: String?, msg: String?): Int {
        val newPriority: Int
        if (priority< ILogDelegate.INFO) {
            newPriority = ILogDelegate.INFO
        } else {
            newPriority = priority
        }
        return Log.println(newPriority, tag ?: javaClass.simpleName, msg ?: "null")
    }

}