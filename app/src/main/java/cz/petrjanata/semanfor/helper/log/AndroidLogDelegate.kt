package cz.petrjanata.semanfor.helper.log

import android.util.Log

/**
 * Created by Petr Janata on 15.03.2018.
 */

internal class AndroidLogDelegate : ILogDelegate {

    override fun v(tag: String?, msg: String?): Int {
        return Log.v(tag, msg)
    }

    override fun v(tag: String?, msg: String?, tr: Throwable?): Int {
        return Log.v(tag, msg, tr)
    }

    override fun d(tag: String?, msg: String?): Int {
        return Log.d(tag, msg)
    }

    override fun d(tag: String?, msg: String?, tr: Throwable?): Int {
        return Log.d(tag, msg, tr)
    }

    override fun i(tag: String?, msg: String?): Int {
        return Log.i(tag, msg)
    }

    override fun i(tag: String?, msg: String?, tr: Throwable?): Int {
        return Log.i(tag, msg, tr)
    }

    override fun w(tag: String?, msg: String?): Int {
        return Log.w(tag, msg)
    }

    override fun w(tag: String?, msg: String?, tr: Throwable?): Int {
        return Log.w(tag, msg, tr)
    }

    override fun isLoggable(s: String?, i: Int): Boolean {
        return Log.isLoggable(s, i)
    }

    override fun w(tag: String?, tr: Throwable?): Int {
        return Log.w(tag, tr)
    }

    override fun e(tag: String?, msg: String?): Int {
        return Log.e(tag, msg)
    }

    override fun e(tag: String?, msg: String?, tr: Throwable?): Int {
        return Log.e(tag, msg, tr)
    }

    override fun wtf(tag: String?, msg: String?): Int {
        return Log.wtf(tag, msg)
    }

    override fun wtf(tag: String?, tr: Throwable?): Int {
        return Log.wtf(tag, tr)
    }

    override fun wtf(tag: String?, msg: String?, tr: Throwable?): Int {
        return Log.wtf(tag, msg, tr)
    }

    override fun getStackTraceString(tr: Throwable?): String {
        return Log.getStackTraceString(tr)
    }

    override fun println(priority: Int, tag: String?, msg: String?): Int {
        return Log.println(priority, tag, msg)
    }
}
