package cz.petrjanata.semanfor.helper.inject

import android.content.SharedPreferences
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.provider
import com.github.salomonbrys.kodein.singleton
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.api.ApiConstants
import cz.petrjanata.semanfor.config.AppConstants
import cz.petrjanata.semanfor.config.ConfigConstants
import cz.petrjanata.semanfor.service.google.SignInHelper
import cz.petrjanata.semanfor.service.web.ApiWebService
import cz.petrjanata.semanfor.service.web.WebServiceHelper
import cz.petrjanata.semanfor.service.AndroidHelper
import cz.petrjanata.semanfor.service.driverinfo.DriverInfoManager
import cz.petrjanata.semanfor.service.web.JsonHelper
import cz.petrjanata.semanfor.usecase.*
import cz.petrjanata.semanfor.view.notification.AndroidNotificationCreator
import cz.petrjanata.semanfor.view.notification.NotificationView

/**
 * Created by Petr Janata on 29.03.2018.
 */
class SemanforKodeinHelper {

    fun provideKodein(): Kodein {
        return Kodein {

            ///constants
            bind<ApiConstants>() with provider { ApiConstants }
            bind<ConfigConstants>() with provider { ConfigConstants }
            bind<AppConstants>() with provider { AppConstants }

            ///use cases
            bind<GetServerAuthCodeUseCase>() with provider { GetServerAuthCodeUseCase() }
            bind<RequestDriverInfoUsecase>() with provider { RequestDriverInfoUsecase() }
            bind<ReceiveDriverInfoUsecase>() with provider { ReceiveDriverInfoUsecase() }
            bind<FirebaseUsecase>() with provider { FirebaseUsecase() }
            bind<DateUsecase>() with provider { DateUsecase() }
            bind<ReservationUsecase>() with provider { ReservationUsecase() }

            ///managers
            bind<DriverInfoManager>() with singleton { DriverInfoManager() }

            ///Android
            ///helpers
            bind<AndroidHelper>() with provider { AndroidHelper() }
            bind<SignInHelper>() with singleton { SignInHelper() }
            bind<WebServiceHelper>() with provider { WebServiceHelper() }
            bind<JsonHelper>() with provider { JsonHelper() }
            bind<AndroidNotificationCreator>() with provider { AndroidNotificationCreator() }

            ///context
            bind<App>() with provider { App.instance }
            bind<SharedPreferences>() with singleton { App.instance.appPreferences }

            ///services
            bind<ApiWebService>() with provider { ApiWebService() }

            ///views
            bind<NotificationView>() with provider { NotificationView() }

        }
    }
}