package cz.petrjanata.semanfor.helper.log

import java.io.PrintWriter
import java.io.StringWriter
import java.net.UnknownHostException

/**
 * Created by Petr Janata on 15.03.2018.
 */

internal abstract class ASemanforLogDelegate : ILogDelegate {


    ///implemented methods
    override fun v(tag: String?, msg: String?): Int {
        return println(ILogDelegate.VERBOSE, tag, msg)
    }

    override fun v(tag: String?, msg: String?, tr: Throwable?): Int {
        return println(ILogDelegate.VERBOSE, tag, "$msg\n${getStackTraceString(tr)}")
    }

    override fun d(tag: String?, msg: String?): Int {
        return println(ILogDelegate.DEBUG, tag, msg)
    }

    override fun d(tag: String?, msg: String?, tr: Throwable?): Int {
        return println(ILogDelegate.DEBUG, tag, "$msg\n${getStackTraceString(tr)}")
    }

    override fun i(tag: String?, msg: String?): Int {
        return println(ILogDelegate.INFO, tag, msg)
    }

    override fun i(tag: String?, msg: String?, tr: Throwable?): Int {
        return println(ILogDelegate.INFO, tag, "$msg\n${getStackTraceString(tr)}")
    }

    override fun w(tag: String?, msg: String?): Int {
        return println(ILogDelegate.WARN, tag, msg)
    }

    override fun w(tag: String?, msg: String?, tr: Throwable?): Int {
        return println(ILogDelegate.WARN, tag, "$msg\n${getStackTraceString(tr)}")
    }

    override fun w(tag: String?, tr: Throwable?): Int {
        return println(ILogDelegate.WARN, tag, getStackTraceString(tr))
    }

    override fun e(tag: String?, msg: String?): Int {
        return println(ILogDelegate.ERROR, tag, msg)
    }

    override fun e(tag: String?, msg: String?, tr: Throwable?): Int {
        return println(ILogDelegate.ERROR, tag, "$msg\n${getStackTraceString(tr)}")
    }

    override fun wtf(tag: String?, msg: String?): Int {
        return println(ILogDelegate.ERROR, tag, msg)
    }

    override fun wtf(tag: String?, tr: Throwable?): Int {
        return println(ILogDelegate.ERROR, tag, getStackTraceString(tr))
    }

    override fun wtf(tag: String?, msg: String?, tr: Throwable?): Int {
        return println(ILogDelegate.ERROR, tag, "$msg\n${getStackTraceString(tr)}")
    }

    override fun getStackTraceString(tr: Throwable?): String {
        if (tr == null) {
            return ""
        }

        // This is to reduce the amount of log spew that apps do in the non-error
        // condition of the network being unavailable.
        var t: Throwable? = tr
        while (t != null) {
            if (t is UnknownHostException) {
                return ""
            }
            t = t.cause
        }

        val sw = StringWriter()
        val pw = PrintWriter(sw)
        tr.printStackTrace(pw)
        pw.flush()
        return sw.toString()
    }


    ///abstract methods
    abstract override fun isLoggable(s: String?, i: Int): Boolean

    abstract override fun println(priority: Int, tag: String?, msg: String?): Int
}
