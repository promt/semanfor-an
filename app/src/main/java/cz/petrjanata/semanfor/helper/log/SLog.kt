package cz.petrjanata.semanfor.helper.log

/**
 * Provides logging for SeManfor application. Using bridge pattern.
 * You can define your own log delegate for different scopes. For
 * example different for production and different for tests.
 * */
object SLog {


    private var logDelegate: ILogDelegate = AndroidLogDelegate()

    fun setScope(scope: Scope) {
        when(scope) {
            Scope.DEBUG -> logDelegate = AndroidLogDelegate()
            Scope.PROD -> logDelegate = AndroidLogDelegate()
            Scope.TEST -> logDelegate = ConsolePrintLogDelegate()
        }
    }

    fun notImplementedYet() {
        val stackTraceElement = Exception().fillInStackTrace().stackTrace[1]
        val methodName = stackTraceElement.methodName
        val className = stackTraceElement.className
        notImplementedYet(className, methodName)
    }

    fun notImplementedYet(methodName: String) {
        notImplementedYet("SeManfor", methodName)
    }

    fun notImplementedYet(className: String, methodName: String) {
        val message = "Method not implemented yet: $methodName"
        w(className, message)
    }

    fun logWalkThrough() {
        val stackTraceElement = Exception().fillInStackTrace().stackTrace[1]
        val methodName = stackTraceElement.methodName
        val className = stackTraceElement.className
        val classSimpleName: String = className.split('.').last()
        v("WalkThrough", "$classSimpleName#$methodName")
    }

    ///instance public methods
    fun v(msg: String?): Int {
        val tag = provideAutoTag()
        return v(tag, msg)
    }

    fun v(msg: String?, tr: Throwable?): Int {
        val tag = provideAutoTag()
        if (tr == null) {
            return v(tag, msg)
        } else {
            return v(tag, msg, tr)
        }
    }

    fun d(msg: String?): Int {
        val tag = provideAutoTag()
        return d(tag, msg)
    }

    fun i(msg: String?): Int {
        val tag = provideAutoTag()
        return i(tag, msg)
    }

    fun i(msg: String?, tr: Throwable): Int {
        val tag = provideAutoTag()
        return i(tag, msg, tr)
    }

    fun w(msg: String?): Int {
        val tag = provideAutoTag()
        return w(tag, msg)
    }

    fun e(msg: String?): Int {
        val tag = provideAutoTag()
        return e(tag, msg)
    }

    fun e(msg: String?, tr: Throwable): Int {
        val tag = provideAutoTag()
        return e(tag, msg, tr)
    }


    ///forwarded methods
    fun v(tag: String?, msg: String?): Int {
        return logDelegate.v(tag, msg)
    }

    fun v(tag: String?, msg: String?, tr: Throwable): Int {
        return logDelegate.v(tag, msg, tr)
    }

    fun d(msg: String?, tr: Throwable): Int {
        val tag = provideAutoTag()
        return d(tag, msg, tr)
    }

    fun d(tag: String?, msg: String?): Int {
        return logDelegate.d(tag, msg)
    }

    fun d(tag: String?, msg: String?, tr: Throwable): Int {
        return logDelegate.d(tag, msg, tr)
    }

    fun i(tag: String?, msg: String?): Int {
        return logDelegate.i(tag, msg)
    }

    fun i(tag: String?, msg: String?, tr: Throwable): Int {
        return logDelegate.i(tag, msg, tr)
    }

    fun w(tag: String?, msg: String?): Int {
        return logDelegate.w(tag, msg)
    }

    fun w(tag: String?, msg: String?, tr: Throwable): Int {
        return logDelegate.w(tag, msg, tr)
    }

    fun isLoggable(s: String?, i: Int): Boolean {
        return logDelegate.isLoggable(s, i)
    }

    fun w(tag: String?, tr: Throwable): Int {
        return logDelegate.w(tag, tr)
    }

    fun e(tag: String?, msg: String?): Int {
        return logDelegate.e(tag, msg)
    }

    fun e(tag: String?, msg: String?, tr: Throwable): Int {
        return logDelegate.e(tag, msg, tr)
    }

    fun wtf(tag: String?, msg: String?): Int {
        return logDelegate.wtf(tag, msg)
    }

    fun wtf(tag: String?, tr: Throwable): Int {
        return logDelegate.wtf(tag, tr)
    }

    fun wtf(tag: String?, msg: String?, tr: Throwable): Int {
        return logDelegate.wtf(tag, msg, tr)
    }

    fun getStackTraceString(tr: Throwable): String {
        return logDelegate.getStackTraceString(tr)
    }

    fun println(priority: Int, tag: String?, msg: String?): Int {
        return logDelegate.println(priority, tag, msg)
    }


    ///private methods
    private fun provideAutoTag(): String {
        val className: String = Exception().fillInStackTrace().stackTrace[2].className
        val classSimpleName: String = className.split('.').last()
        return classSimpleName
    }


    ///Inner class
    public enum class Scope {
        DEBUG,
        PROD,
        TEST
    }
}