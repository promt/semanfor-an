package cz.petrjanata.semanfor.helper

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors

fun <T> T.executeAsync(task: ()->Unit) {
    ExecutionHelper.bgExecutor.execute(task)
}

fun <T> T.executeUi(task: ()->Unit) {
    ExecutionHelper.uiExecutor.execute(task)
}

private object ExecutionHelper {
    val bgExecutor: Executor by lazy { Executors.newCachedThreadPool() }
    val uiExecutor: Executor by lazy {
        Executor { command ->
            Handler(Looper.getMainLooper()).post(command)
        }
    }
}

