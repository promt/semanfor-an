package cz.petrjanata.semanfor.helper.log


/**
 * Created by Petr Janata on 15.03.2018.
 */

internal interface ILogDelegate {

    fun v(tag: String?, msg: String?): Int

    fun v(tag: String?, msg: String?, tr: Throwable?): Int

    fun d(tag: String?, msg: String?): Int

    fun d(tag: String?, msg: String?, tr: Throwable?): Int

    fun i(tag: String?, msg: String?): Int

    fun i(tag: String?, msg: String?, tr: Throwable?): Int

    fun w(tag: String?, msg: String?): Int

    fun w(tag: String?, msg: String?, tr: Throwable?): Int

    fun isLoggable(s: String?, i: Int): Boolean

    fun w(tag: String?, tr: Throwable?): Int

    fun e(tag: String?, msg: String?): Int

    fun e(tag: String?, msg: String?, tr: Throwable?): Int

    fun wtf(tag: String?, msg: String?): Int

    fun wtf(tag: String?, tr: Throwable?): Int

    fun wtf(tag: String?, msg: String?, tr: Throwable?): Int

    fun getStackTraceString(tr: Throwable?): String

    fun println(priority: Int, tag: String?, msg: String?): Int

    companion object {

        /**
         * Priority constant for the println method; use SLog.v.
         */
        val VERBOSE = 2

        /**
         * Priority constant for the println method; use SLog.d.
         */
        val DEBUG = 3

        /**
         * Priority constant for the println method; use SLog.i.
         */
        val INFO = 4

        /**
         * Priority constant for the println method; use SLog.w.
         */
        val WARN = 5

        /**
         * Priority constant for the println method; use SLog.e.
         */
        val ERROR = 6

        /**
         * Priority constant for the println method.
         */
        val ASSERT = 7
    }
}
