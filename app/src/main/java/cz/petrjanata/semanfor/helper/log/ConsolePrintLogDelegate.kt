package cz.petrjanata.semanfor.helper.log

/**
 * Created by Petr Janata on 15.03.2018.
 */

internal class ConsolePrintLogDelegate : ASemanforLogDelegate() {

    override fun isLoggable(s: String?, i: Int): Boolean {
        return true
    }

    override fun println(priority: Int, tag: String?, msg: String?): Int {
        if (priority >= ILogDelegate.Companion.WARN) {
            System.err.println("$tag: $msg")
        } else {
            System.out.println("$tag: $msg")
        }

        return 0
    }
}
