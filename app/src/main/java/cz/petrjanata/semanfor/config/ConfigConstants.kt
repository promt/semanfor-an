package cz.petrjanata.semanfor.config

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Petr Janata on 02.04.2018.
 */
object ConfigConstants : BaseConfigConstants() {

    const val GPS_PARKING_LATITUDE = 50.1109194
    const val GPS_PARKING_LONGITUDE = 14.4468356
    const val GPS_PARKING_RADIUS = 1000F //in meters
    const val BACKEND_URL = "https://immense-everglades-39500.herokuapp.com"
          val TOAST_DATE_FORMAT: SimpleDateFormat = SimpleDateFormat("dd.MM.", Locale.US)
    const val RESERVATION_PROLONGATION: Long = 24*60*60*1000
    const val MORNING = 8

}