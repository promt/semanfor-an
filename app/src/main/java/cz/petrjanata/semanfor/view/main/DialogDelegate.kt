package cz.petrjanata.semanfor.view.main

import android.app.DatePickerDialog
import android.arch.lifecycle.Observer
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.widget.DatePicker
import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.R
import cz.petrjanata.semanfor.usecase.DateUsecase
import java.util.*


class DialogDelegate(private val activity: MainActivity) {

    private val dateUsecase: DateUsecase = App.kodein.instance()

    private lateinit var viewModel: MainViewModel


    fun initViewModel(mainViewModel: MainViewModel) {
        viewModel = mainViewModel
        viewModel.getDialogMessage().observe(activity, Observer { message: String? ->
            message?.let { openMessageDialog(it) }
        })
        viewModel.getReservationDialogVisible().observe(activity, Observer { visible: Boolean? ->
            visible ?: throw viewModel.knownIssue()
            if (visible) {
                openReservationDialog()
            }
        })
        viewModel.getCalendarDialogState().observe(activity, Observer { state: DatePickerStateVM? ->
            state ?: throw viewModel.knownIssue()
            if (state.visible) {
                openCalendarDialog(state)
            }
        })

    }

    fun showMessageDialog(message: String) {
        viewModel.setDialogMessage(message)
    }

    fun showReservationDialog() {
        viewModel.setReservationDialogVisible(true)
    }


    private fun openMessageDialog(message: String) {
        if (message.isEmpty().or(message.isBlank())) {
            return
        }

        val builder = AlertDialog.Builder(activity)
        builder.setMessage(message)
        builder.setCancelable(true)
        builder.setOnCancelListener {
            viewModel.setDialogMessage(null)
        }

        val alertDialog = builder.create()
        alertDialog.show()
    }

    private fun openReservationDialog() {
        val onOkClickListener = DialogInterface.OnClickListener { dialog, which ->
            onReservationDialogCancel(null)
            viewModel.setCalendarDialogState(DatePickerStateVM(true))
        }

        val builder = AlertDialog.Builder(activity)
        builder.setTitle(R.string.title_reservation_dialog)
                .setMessage(R.string.message_reservation_dialog)
                .setCancelable(true)
                .setPositiveButton(android.R.string.ok, onOkClickListener)
                .setNegativeButton(android.R.string.cancel, this::onReservationDialogCancel)
                .setOnCancelListener(this::onReservationDialogCancel)

        val alertDialog = builder.create()
        alertDialog.show()
    }

    private fun onReservationDialogCancel(dialog:DialogInterface?) {
        onReservationDialogCancel(dialog, 0)
    }

    private fun onReservationDialogCancel(dialog:DialogInterface?, which: Int) {
        viewModel.setReservationDialogVisible(false)
    }


    private fun openCalendarDialog(state: DatePickerStateVM) {
        val minDate: Date = dateUsecase.getMinReservationDate()

        if (state.selectedDate < minDate) {
            state.selectedDate = minDate
        }

        val preSelected = Calendar.getInstance()
        preSelected.time = state.selectedDate

        val datePickerDialog: DatePickerDialog = object : DatePickerDialog(activity,
                this::onCalendarDatePickedUp,
                preSelected.get(Calendar.YEAR),
                preSelected.get(Calendar.MONTH),
                preSelected.get(Calendar.DAY_OF_MONTH)) {

            override fun onDateChanged(view: DatePicker, year: Int, month: Int, dayOfMonth: Int) {
                super.onDateChanged(view, year, month, dayOfMonth)
                val cal = Calendar.getInstance()
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, month)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                state.selectedDate = cal.time
            }
        }

        datePickerDialog.setOnCancelListener { onCalendarCancel() }
        datePickerDialog.datePicker.minDate = minDate.time

        datePickerDialog.show()
    }

    private fun onCalendarCancel() {
        viewModel.setCalendarDialogState(DatePickerStateVM(false))
    }

    private fun onCalendarDatePickedUp(view: DatePicker, year: Int, month: Int, dayOfMonth: Int) {
        val cal = Calendar.getInstance()
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        onCalendarCancel()

        activity.openReservationActivity(cal.time)
    }

}