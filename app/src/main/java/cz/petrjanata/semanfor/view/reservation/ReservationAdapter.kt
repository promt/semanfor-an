package cz.petrjanata.semanfor.view.reservation

import cz.petrjanata.semanfor.R
import cz.petrjanata.semanfor.view.BasePlacesAdapter
import java.util.*

class ReservationAdapter : BasePlacesAdapter {

    public override lateinit var dateOfReservation: Date

    protected override val labelAllocateRes: Int = R.string.label_reserve_place

    constructor(onAllocationEdited: (String) -> Unit, indicateLoading: (Boolean) -> Unit)
            : super(onAllocationEdited, indicateLoading)


}