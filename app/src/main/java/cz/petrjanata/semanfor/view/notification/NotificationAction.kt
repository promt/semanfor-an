package cz.petrjanata.semanfor.view.notification

import cz.petrjanata.semanfor.config.AppConstants

class NotificationAction(val title: String,
                         val type: AppConstants.NotificationAction,
                         val extras: String? = null)