package cz.petrjanata.semanfor.view.main

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import cz.petrjanata.semanfor.R

class PlaceViewHolder : RecyclerView.ViewHolder {
    val rootView: View
    val captionTV: TextView
    val labelTV: TextView
    val descriptionTV: TextView
    val arrow: View

    constructor(rootView: View) : super(rootView) {
        this.rootView = rootView
        captionTV = rootView.findViewById(R.id.captionTV)
        labelTV = rootView.findViewById(R.id.labelTV)
        descriptionTV = rootView.findViewById(R.id.descriptionTV)
        arrow = rootView.findViewById(R.id.arrow)
    }
}
