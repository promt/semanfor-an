package cz.petrjanata.semanfor.view.model

import cz.petrjanata.semanfor.api.model.DriverADO
import cz.petrjanata.semanfor.api.model.PlaceADO

class PlaceVM(
        val caption: String,
        val name: String,
        val calendarID: String,
        val driver: DriverADO?,
        val state: State
) {

    constructor(ado: PlaceADO, state: State) : this(
            ado.caption,
            ado.name,
            ado.calendarID,
            ado.driver,
            state
    )

    enum class State {
        FREE, FULL, MAIN
    }
}