package cz.petrjanata.semanfor.view.reservation

import android.arch.lifecycle.LiveData
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.model.SemanforException
import cz.petrjanata.semanfor.service.driverinfo.DriverInfoListenerAdapter
import cz.petrjanata.semanfor.service.driverinfo.DriverInfoManager
import cz.petrjanata.semanfor.view.BaseDriverInfoViewModel
import cz.petrjanata.semanfor.view.model.DriverInfoVM
import java.util.*

class ReservationViewModel : BaseDriverInfoViewModel() {

    protected override val driverInfoListener: DriverInfoManager.DriverInfoListener = DriverInfoListenerAdapter(DriverInfoManager.ListenerType.RES_ACTIVITY, this::postDriverInfo)

    fun getDriverInfo(day: Date): LiveData<IValueWrapper<DriverInfoVM, SemanforException>> {
        return baseGetDriverInfo(day)
    }

    fun reloadDriverInfo(day: Date) {
        baseReloadDriverInfo(day)
    }
}
