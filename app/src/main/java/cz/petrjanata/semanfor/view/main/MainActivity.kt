package cz.petrjanata.semanfor.view.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.R
import cz.petrjanata.semanfor.config.AppConstants
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.model.SemanforException
import cz.petrjanata.semanfor.service.google.SignInHelper
import cz.petrjanata.semanfor.usecase.DateUsecase
import cz.petrjanata.semanfor.view.BasePlacesActivity
import cz.petrjanata.semanfor.view.SettingsActivity
import cz.petrjanata.semanfor.view.SleepingActivity
import cz.petrjanata.semanfor.view.model.DriverInfoVM
import cz.petrjanata.semanfor.view.reservation.ReservationActivity
import cz.petrjanata.semanfor.view.signin.SignInActivity
import kotlinx.android.synthetic.main.activity_place_list.*
import java.util.*


class MainActivity : BasePlacesActivity() {

    private val signInHelper: SignInHelper = App.kodein.instance()
    private val appConstants: AppConstants = App.kodein.instance()
    private val dateUsecase: DateUsecase = App.kodein.instance()

    private lateinit var mainViewModel: MainViewModel
    protected override val placesAdapter: PlacesAdapter = PlacesAdapter(this::onAllocationEdited, this::setLoading)
    private val dialogDelegate: DialogDelegate = DialogDelegate(this)

    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    protected override fun initViewModel() {
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        dialogDelegate.initViewModel(mainViewModel)
        mainViewModel.getDriverInfo().observe(this, Observer<IValueWrapper<DriverInfoVM, SemanforException>> { wrapper: IValueWrapper<DriverInfoVM, SemanforException>? ->
            wrapper ?: throw mainViewModel.knownIssue()
            updateDriverInfo(wrapper)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main_activity_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_new_reservation -> {
                dialogDelegate.showReservationDialog()
                return true
            }
            R.id.action_reload -> {
                refreshView.isRefreshing = true
                loadPlaces()
                return true
            }
            R.id.action_settings -> {
                startActivity(SettingsActivity::class)
                return true
            }
            R.id.action_logout -> {
                signOut()
                return false
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        if (dateUsecase.isAppSleeping()) {
            switchToSleepingActivity()
        }
    }

    ///other non-private methods
    internal fun openReservationActivity(day: Date) {
        val intent = Intent(this, ReservationActivity::class.java)
        intent.putExtra(appConstants.RESERVATION_DATE_EXTRA_KEY, day)
        startActivity(intent)
    }

    ///private methods
    private fun switchToSleepingActivity(): Unit {
        val intent = Intent(activity, SleepingActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    private fun signOut() {
        signInHelper.signOut {
            val intent = Intent(activity, SignInActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }

    protected override fun loadPlaces() {
        mainViewModel.reloadDriverInfo()
        errorView.visibility = View.GONE
    }


    ///callbacks
    protected override fun onAllocationEdited(message: String) {
        dialogDelegate.showMessageDialog(message)
    }

}
