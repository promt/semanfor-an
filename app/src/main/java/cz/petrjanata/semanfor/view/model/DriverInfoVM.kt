package cz.petrjanata.semanfor.view.model

import cz.petrjanata.semanfor.api.model.AllocationADO
import cz.petrjanata.semanfor.api.model.LotState

class DriverInfoVM(
        val lotState: LotState,
        val places: List<PlaceVM>,
        val allocation: AllocationADO? = null)