package cz.petrjanata.semanfor.view.notification

import android.graphics.Bitmap

interface NotificationBox {

    abstract val channel: BoxChannel
    abstract val largeIcon: Bitmap?
    abstract val title: String
    /**notificationID is a unique int for each notification that you must define*/
    abstract val notificationID: Int
    val actions: Array<NotificationAction>
        get () = emptyArray()

}