package cz.petrjanata.semanfor.view.notification

import android.support.annotation.ColorInt

interface BoxChannel {
    @get:ColorInt
    abstract val ledColor: Int?
    abstract val channelId: String
    abstract val channelName: String
    abstract val channelDescription: String
}