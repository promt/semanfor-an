package cz.petrjanata.semanfor.view.main

import cz.petrjanata.semanfor.R
import cz.petrjanata.semanfor.view.BasePlacesAdapter
import java.util.*


class PlacesAdapter : BasePlacesAdapter {

    protected override val labelAllocateRes: Int = R.string.label_park_here
    protected override val dateOfReservation: Date? = null

    constructor(onAllocationEdited: (String) -> Unit, indicateLoading: (Boolean) -> Unit)
            : super(onAllocationEdited, indicateLoading)

}