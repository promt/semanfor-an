package cz.petrjanata.semanfor.view

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.support.annotation.WorkerThread
import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.api.model.DriverInfoADO
import cz.petrjanata.semanfor.api.model.PlaceADO
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.model.SemanforException
import cz.petrjanata.semanfor.model.ValueWrapper
import cz.petrjanata.semanfor.service.driverinfo.DriverInfoManager
import cz.petrjanata.semanfor.usecase.RequestDriverInfoUsecase
import cz.petrjanata.semanfor.view.model.DriverInfoVM
import cz.petrjanata.semanfor.view.model.PlaceVM
import java.util.*

abstract class BaseDriverInfoViewModel : BaseSemanforViewModel() {

    protected abstract val driverInfoListener: DriverInfoManager.DriverInfoListener

    private val requestUsecase: RequestDriverInfoUsecase = App.kodein.instance()

    private lateinit var driverInfo: MutableLiveData<IValueWrapper<DriverInfoVM, SemanforException>>
    private val dialogMessage: MutableLiveData<String?> = MutableLiveData()

    ///live data getters and setters
    protected fun baseGetDriverInfo(day: Date?): LiveData<IValueWrapper<DriverInfoVM, SemanforException>> {
        if (!this::driverInfo.isInitialized) {
            driverInfo = MutableLiveData()
            requestUsecase.registerListener(driverInfoListener)
            baseReloadDriverInfo(day)
        }
        return driverInfo
    }

    @WorkerThread
    fun postDriverInfo(wrapperADO: IValueWrapper<DriverInfoADO, SemanforException>) {
        val diVM = convertDriverInfo(wrapperADO.value)
        val wrapperVM: IValueWrapper<DriverInfoVM, SemanforException> = ValueWrapper(diVM, wrapperADO.exception)

        driverInfo.postValue(wrapperVM)
    }

    fun getDialogMessage(): LiveData<String?> {
        return dialogMessage
    }

    fun setDialogMessage(message: String?) {
        dialogMessage.value = message
    }


    ///other public methods
    protected fun baseReloadDriverInfo(day: Date?) {
        requestUsecase.requestDriverInfo(day)
    }


    protected override fun onCleared() {
        super.onCleared()
        requestUsecase.unregisterListener(driverInfoListener)
    }

    //todo move conversion to usecase
    private fun convertDriverInfo(diAdo: DriverInfoADO?) : DriverInfoVM? {
        diAdo ?: return null

        val fullPlaces: ArrayList<PlaceADO> = ArrayList(diAdo.fullPlaces ?: emptyList())
        fullPlaces.sort()
        val freePlaces: ArrayList<PlaceADO> = ArrayList(diAdo.freePlaces ?: emptyList())
        freePlaces.sort()

        val allPlaces: ArrayList<PlaceVM> = ArrayList(fullPlaces.size + freePlaces.size)

        freePlaces.forEach {
            allPlaces.add(PlaceVM(it, PlaceVM.State.FREE))
        }

        fullPlaces.forEach { placeADO ->
            val isMain: Boolean = placeADO.calendarID == diAdo.allocation?.place?.calendarID
            val state: PlaceVM.State
            if (isMain) {
                state = PlaceVM.State.MAIN
            } else {
                state = PlaceVM.State.FULL
            }
            val vm = PlaceVM(placeADO, state)

            allPlaces.add(vm)
        }

        return DriverInfoVM(diAdo.lotState, allPlaces, diAdo.allocation)
    }

}