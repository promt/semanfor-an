package cz.petrjanata.semanfor.view.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.model.SemanforException
import cz.petrjanata.semanfor.service.driverinfo.DriverInfoListenerAdapter
import cz.petrjanata.semanfor.service.driverinfo.DriverInfoManager
import cz.petrjanata.semanfor.view.BaseDriverInfoViewModel
import cz.petrjanata.semanfor.view.model.DriverInfoVM

class MainViewModel : BaseDriverInfoViewModel() {


    protected override val driverInfoListener: DriverInfoManager.DriverInfoListener = DriverInfoListenerAdapter(DriverInfoManager.ListenerType.MAIN_ACTIVITY, this::postDriverInfo)

    private val dialogReservationVisible: MutableLiveData<Boolean> = MutableLiveData<Boolean>()
    private val dialogCalendarState: MutableLiveData<DatePickerStateVM> = MutableLiveData<DatePickerStateVM>().apply { value = DatePickerStateVM(false) }


    ///live data getters and setters
    fun getDriverInfo(): LiveData<IValueWrapper<DriverInfoVM, SemanforException>> {
        return baseGetDriverInfo(null)
    }

    fun getReservationDialogVisible(): LiveData<Boolean> {
        return dialogReservationVisible
    }

    fun setReservationDialogVisible(visible: Boolean) {
        dialogReservationVisible.value = visible
    }

    fun getCalendarDialogState(): LiveData<DatePickerStateVM> {
        return dialogCalendarState
    }

    fun setCalendarDialogState(state: DatePickerStateVM) {
        dialogCalendarState.value = state
    }

    ///other public methods
    fun reloadDriverInfo() {
        baseReloadDriverInfo(null)
    }
}