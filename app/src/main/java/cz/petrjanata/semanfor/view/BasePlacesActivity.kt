package cz.petrjanata.semanfor.view

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.R
import cz.petrjanata.semanfor.api.model.LotState
import cz.petrjanata.semanfor.config.ConfigConstants
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.model.SemanforException
import cz.petrjanata.semanfor.view.model.DriverInfoVM
import kotlinx.android.synthetic.main.activity_place_list.*

abstract class BasePlacesActivity : BaseSemanforActivity() {

    protected abstract val placesAdapter: BasePlacesAdapter

    private val configConstants: ConfigConstants = App.kodein.instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place_list)
        initData()
        initToolbar()
        initView()
        initViewModel()
    }

    protected open fun initData() {
        //do nothing
    }

    protected fun initView() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = placesAdapter
        refreshView.setOnRefreshListener { loadPlaces() }
        refreshView.isRefreshing = true
    }

    protected abstract fun initViewModel()

    protected abstract fun loadPlaces()

    protected fun updateDriverInfo(wrapper: IValueWrapper<DriverInfoVM, SemanforException>) {
        refreshView.isRefreshing = false
        wrapper.validate()

        wrapper.exception?.let {
            val errorLabel: String = resources.getString(R.string.error_there_was_an_error_while_loading_data)
            var message = ""
            if (configConstants.BUILD_DEBUG) {
                message = "\n" + it.message
            }

            errorView.text = "$errorLabel$message"
            errorView.visibility = View.VISIBLE
            placesAdapter.setNewData(LotState.FREE, emptyList(), null)
            return@updateDriverInfo
        }

        errorView.visibility = View.GONE
        val diVM: DriverInfoVM = wrapper.value ?: throw wrapper.illegalStateException
        placesAdapter.setNewData(diVM.lotState, diVM.places, diVM.allocation)
    }


    ///callbacks
    protected abstract fun onAllocationEdited(message: String)

    protected fun setLoading(isLoading: Boolean) {
        refreshView.isRefreshing = isLoading
    }
}