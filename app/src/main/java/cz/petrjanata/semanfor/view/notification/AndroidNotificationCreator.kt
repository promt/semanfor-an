package cz.petrjanata.semanfor.view.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.R
import cz.petrjanata.semanfor.config.AppConstants
import cz.petrjanata.semanfor.service.receiver.NotificationActionBroadcastReceiver
import cz.petrjanata.semanfor.view.signin.SignInActivity

class AndroidNotificationCreator {

    private val appConstants: AppConstants = App.kodein.instance()

    fun createNotification(context: Context, notificationBox: NotificationBox) {

        val smallIconRes: Int = R.drawable.ic_notification_status_bar_white
        val notificationColor: Int = context.resources.getColor(R.color.colorAccent)
        val contentText: String = context.resources.getString(R.string.notif_label_click_to_open_app)

        val resultIntent = Intent(context, SignInActivity::class.java)
        // Because clicking the notification opens a new ("special") activity, there's
        // no need to create an artificial back stack.
        val resultPendingIntent = PendingIntent.getActivity(
                context,
                0,
                resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        )

        val channelId: String = notificationBox.channel.channelId
        val largeIconBitmap: Bitmap? = notificationBox.largeIcon


        val builder = NotificationCompat.Builder(context, channelId)
                .apply {
                    setSmallIcon(smallIconRes)
                    setContentTitle(notificationBox.title)
                    setContentText(contentText)
                    setLargeIcon(largeIconBitmap)
                    setChannelId(channelId)
                    setColor(notificationColor)
                    setContentIntent(resultPendingIntent)
                    setAutoCancel(true)
                    setPriority(NotificationCompat.PRIORITY_HIGH)
                    setDefaults(Notification.DEFAULT_ALL)
                }

        notificationBox.actions.forEach { action ->
            builder.addAction(buildAction(context, action, notificationBox.notificationID))
        }


        val channelsManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationManager = NotificationManagerCompat.from(context)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = buildChannel(notificationBox.channel)
            channelsManager.createNotificationChannel(channel)
        }


        // notificationId is a unique int for each notification that you must define
        val notification: Notification = builder.build()
        notificationManager.notify(notificationBox.notificationID, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun buildChannel(boxChannel: BoxChannel): NotificationChannel {

        val importance: Int = NotificationManager.IMPORTANCE_HIGH
        val channel = NotificationChannel(boxChannel.channelId, boxChannel.channelName, importance)
        channel.apply {
            enableLights(true)
            enableVibration(true)
            description = boxChannel.channelDescription
            boxChannel.ledColor?.let { lightColor = it }
        }

        return channel
    }

    private fun buildAction(context: Context, action: NotificationAction, notificationID: Int): NotificationCompat.Action {
        val intent = Intent(context, NotificationActionBroadcastReceiver::class.java)
        val actionType = action.type
        intent.putExtra(appConstants.NOTIFICATION_ID_EXTRA_KEY, notificationID)
        intent.putExtra(appConstants.NOTIFICATION_ACTION_TYPE_EXTRA_KEY, actionType.name)
        intent.putExtra(actionType.name, action.extras)

        val pendingIntent: PendingIntent = PendingIntent.getBroadcast(context, actionType.ordinal, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        return NotificationCompat.Action(0, action.title, pendingIntent)
    }
}