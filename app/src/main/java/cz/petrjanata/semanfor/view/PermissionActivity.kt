package cz.petrjanata.semanfor.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import cz.petrjanata.semanfor.helper.log.SLog

class PermissionActivity : AppCompatActivity() {

    companion object {

        private val ARG_PERMISSIONS = "ARG_PERMISSIONS"

        /**
         * Creates intent
         *
         * @param context        context
         * @param permission one permission that is requested
         *
         * @return a prepared intent
         */
        fun getIntent(context: Context, permission: String): Intent {
            return getIntent(context, arrayOf(permission))
        }

        /**
         * Creates intent
         *
         * @param context         context
         * @param permissions list of permissions that are requested
         *
         * @return a prepared intent
         */
        fun getIntent(context: Context, permissions: Array<String>): Intent {
            val intent = Intent(context, PermissionActivity::class.java)
            intent.putExtra(ARG_PERMISSIONS, permissions)
            return intent
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val perms: Array<String>

        if (intent == null
                || intent.extras == null
                || !intent.extras.containsKey(ARG_PERMISSIONS)) {

            throw IllegalArgumentException("Missing permission argument.")
        }

        perms = intent.extras.getStringArray(ARG_PERMISSIONS)
        if (perms.isEmpty()) {
            throw IllegalArgumentException("Permission request is not valid - $ARG_PERMISSIONS array is empty")
        }

        requestPermissions(perms, 0)

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        for (grantResult in grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                SLog.w("One or more permission were not granted, finishing")
                setResult(Activity.RESULT_CANCELED)
                finish()
                return
            }
        }

        setResult(Activity.RESULT_OK)
        finish()
    }

}