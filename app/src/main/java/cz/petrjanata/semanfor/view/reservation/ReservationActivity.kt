package cz.petrjanata.semanfor.view.reservation

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.R
import cz.petrjanata.semanfor.config.AppConstants
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.model.SemanforException
import cz.petrjanata.semanfor.view.BasePlacesActivity
import cz.petrjanata.semanfor.view.model.DriverInfoVM
import kotlinx.android.synthetic.main.activity_place_list.*
import java.util.*

class ReservationActivity : BasePlacesActivity() {

    private val appConstants: AppConstants = App.kodein.instance()

    private lateinit var dateOfReservation: Date
    private lateinit var reservationViewModel: ReservationViewModel
    protected override val placesAdapter: ReservationAdapter = ReservationAdapter(this::onAllocationEdited, this::setLoading)
    private val dialogDelegate: DialogDelegate = DialogDelegate(this)


    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            // Respond to the action bar's Up/Home/back button
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    protected override fun initData() {
        dateOfReservation = intent.getSerializableExtra(appConstants.RESERVATION_DATE_EXTRA_KEY) as Date
        placesAdapter.dateOfReservation = dateOfReservation
    }

    protected override fun initToolbar() {
        super.initToolbar()
        val actionBar = this.supportActionBar ?: throw IllegalStateException("Actionbar is not initialized by toolbar.")
        actionBar.setHomeAsUpIndicator(R.drawable.ic_remove)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }

    protected override fun initViewModel() {
        reservationViewModel = ViewModelProviders.of(this).get(ReservationViewModel::class.java)
        dialogDelegate.initViewModel(reservationViewModel)
        reservationViewModel.getDriverInfo(dateOfReservation).observe(this, Observer<IValueWrapper<DriverInfoVM, SemanforException>> { wrapper: IValueWrapper<DriverInfoVM, SemanforException>? ->
            wrapper ?: throw reservationViewModel.knownIssue()
            updateDriverInfo(wrapper)
        })
    }

    protected override fun loadPlaces() {
        reservationViewModel.reloadDriverInfo(dateOfReservation)
        errorView.visibility = View.GONE
    }

    ///callbacks
    protected override fun onAllocationEdited(message: String) {
        dialogDelegate.showMessageDialog(message)
    }

}