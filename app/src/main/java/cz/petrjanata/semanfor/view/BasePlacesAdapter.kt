package cz.petrjanata.semanfor.view

import android.content.Intent
import android.net.Uri
import android.support.annotation.DrawableRes
import android.support.annotation.LayoutRes
import android.support.annotation.StringRes
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.R
import cz.petrjanata.semanfor.api.model.AllocationADO
import cz.petrjanata.semanfor.api.model.LotState
import cz.petrjanata.semanfor.config.ConfigConstants
import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.model.ConnectionException
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.service.setText
import cz.petrjanata.semanfor.service.setTextColorRes
import cz.petrjanata.semanfor.usecase.ReservationUsecase
import cz.petrjanata.semanfor.view.main.PlaceViewHolder
import cz.petrjanata.semanfor.view.model.PlaceVM
import java.util.*

abstract class BasePlacesAdapter : RecyclerView.Adapter<PlaceViewHolder> {


    @get:StringRes
    protected abstract val labelAllocateRes: Int
    protected abstract val dateOfReservation: Date?


    private val onAllocationEdited: (String) -> Unit
    private val indicateLoading: (Boolean) -> Unit

    private val reservationUsecase: ReservationUsecase = App.kodein.instance()
    private val configConstants: ConfigConstants = App.kodein.instance()
    private val appContext: App = App.kodein.instance()

    @LayoutRes
    private val LAYOUT = R.layout.item_place
    @DrawableRes
    private val RED_CIRCLE_RES = R.drawable.red_circle
    @DrawableRes
    private val GREEN_CIRCLE_RES = R.drawable.green_circle

    private val allPlaces: MutableList<PlaceVM> = mutableListOf()
    private var lotState: LotState = LotState.FREE
    private var allocation: AllocationADO? = null


    constructor(onAllocationEdited: (String) -> Unit, indicateLoading: (Boolean) -> Unit) : super() {
        this.onAllocationEdited = onAllocationEdited
        this.indicateLoading = indicateLoading
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceViewHolder {
        val placeView = View.inflate(parent.context, LAYOUT, null)
        return PlaceViewHolder(placeView)
    }

    override fun getItemCount(): Int {
        return allPlaces.size
    }

    override fun onBindViewHolder(holder: PlaceViewHolder, position: Int): Unit {
        val placeVM = allPlaces[position]

        holder.captionTV.text = placeVM.caption
        when (placeVM.state) {
            PlaceVM.State.FREE -> onBindFreePlace(holder, placeVM)
            PlaceVM.State.FULL -> onBindFullPlace(holder, placeVM)
            PlaceVM.State.MAIN -> onBindMinePlace(holder, placeVM)
            else -> throw IllegalArgumentException("Unsupported state ${placeVM.state}")
        }
    }


    fun setNewData(lotState: LotState, places: List<PlaceVM>, allocation: AllocationADO?) {
        this.lotState = lotState
        this.allocation = allocation
        allPlaces.clear()
        allPlaces.addAll(places)
        this.notifyDataSetChanged()
    }


    ///private methods
    private fun onBindFreePlace(holder: PlaceViewHolder, placeVM: PlaceVM) {
        holder.captionTV.setBackgroundResource(GREEN_CIRCLE_RES)

        if (lotState == LotState.RESERVATION) {
            holder.labelTV.setText(R.string.label_already_parking_somewher_else)
            holder.arrow.visibility = View.INVISIBLE
        } else {
            holder.labelTV.setText(labelAllocateRes)
            holder.arrow.visibility = View.VISIBLE
        }

        holder.descriptionTV.visibility = View.GONE
        holder.descriptionTV.setTextColorRes(R.color.color_blue_grey)
        holder.rootView.setOnClickListener {
            indicateLoading(true)
            reservationUsecase.createAllocation(dateOfReservation, placeVM.calendarID, this::onAllocationCreatedResponse)
        }
    }

    private fun onBindFullPlace(holder: PlaceViewHolder, placeVM: PlaceVM) {
        holder.captionTV.setBackgroundResource(RED_CIRCLE_RES)
        val driver = placeVM.driver
        holder.labelTV.text = driver?.driverName ?: appContext.resources.getString(R.string.label_place_taken)
        holder.arrow.visibility = View.INVISIBLE
        holder.descriptionTV.setTextColorRes(R.color.color_blue_grey)

        if (driver == null) {
            holder.descriptionTV.visibility = View.GONE
            holder.rootView.setOnClickListener(null)
        } else {
            holder.descriptionTV.visibility = View.VISIBLE
            holder.descriptionTV.setText(R.string.label_call)
            holder.rootView.setOnClickListener { rootView ->
                val phoneIntent = Intent(Intent.ACTION_DIAL)
                phoneIntent.data = Uri.parse("tel: ${driver.phoneNumber}")
                rootView.context.startActivity(phoneIntent)
            }
        }
    }

    private fun onBindMinePlace(holder: PlaceViewHolder, placeVM: PlaceVM) {
        holder.captionTV.setBackgroundResource(RED_CIRCLE_RES)
        val driverName = placeVM.driver?.driverName
        SLog.v("My name is $driverName")
        val allocationVal = allocation ?: throw IllegalStateException("There has to be some allocation for place ${placeVM.calendarID}. See conversion driver info ADO to VM.")

        holder.labelTV.setText(R.string.label_name_me, driverName ?: "")
        holder.descriptionTV.setText(R.string.label_release_place)
        holder.descriptionTV.visibility = View.VISIBLE
        holder.descriptionTV.setTextColorRes(R.color.color_app_red)
        holder.rootView.setOnClickListener {
            indicateLoading(true)
            reservationUsecase.releaseAllocation(allocationVal, this::onAllocationCanceledResponse)
        }
    }

    private fun onAllocationCreatedResponse(wrapper: IValueWrapper<AllocationADO, ConnectionException>) {
        indicateLoading(false)

        val message: String
        val allocation = wrapper.value
        if (allocation != null) {

            val dateString: String = configConstants.TOAST_DATE_FORMAT.format(allocation.until)
            val placeCaption: String = allocation.place.caption
            message = appContext.resources.getString(R.string.message_place_reserved, placeCaption, dateString)
        } else {
            var messagePostfix = ""
            if (configConstants.BUILD_DEBUG) {
                val conEx = wrapper.exception!!
                messagePostfix = "\n" + conEx.message
                SLog.d("Allocation creating error: ", conEx)
            }
            message = appContext.resources.getString(R.string.error_place_was_not_allocated) + messagePostfix
        }

        onAllocationEdited.invoke(message)
    }

    private fun onAllocationCanceledResponse(conEx: ConnectionException?) {
        indicateLoading(false)

        val message: String
        if (conEx != null) {
            var messagePostfix = ""
            if (configConstants.BUILD_DEBUG) {
                messagePostfix = "\n" + conEx.message
                SLog.d("Allocation releasing error: ", conEx)
            }
            message = appContext.resources.getString(R.string.error_place_was_not_released) + messagePostfix
        } else {
            message = appContext.resources.getString(R.string.message_place_released)
        }

        onAllocationEdited.invoke(message)
    }

}