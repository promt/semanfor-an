package cz.petrjanata.semanfor.view.notification

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.support.annotation.DrawableRes
import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.R
import cz.petrjanata.semanfor.api.model.AllocationADO
import cz.petrjanata.semanfor.api.model.DriverInfoADO
import cz.petrjanata.semanfor.api.model.LotState
import cz.petrjanata.semanfor.api.model.PlaceADO
import cz.petrjanata.semanfor.config.AppConstants
import cz.petrjanata.semanfor.config.ConfigConstants
import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.model.ConnectionException
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.model.SemanforException
import cz.petrjanata.semanfor.service.AndroidHelper
import cz.petrjanata.semanfor.service.web.JsonHelper

class NotificationView {

    private val notificationCreator: AndroidNotificationCreator = App.kodein.instance()
    private val context: Context = App.kodein.instance<App>()
    private val androidHelper: AndroidHelper = App.kodein.instance()
    private val jsonHelper: JsonHelper = App.kodein.instance()
    private val appConstants: AppConstants = App.kodein.instance()
    private val configConstants: ConfigConstants = App.kodein.instance()

    @DrawableRes
    private val GREEN_CIRCLE = R.drawable.green_circle
    @DrawableRes
    private val RED_CIRCLE = R.drawable.red_circle
    @DrawableRes
    private val BLACK_CIRCLE = R.drawable.black_circle


    fun onAllocationResponse(wrapper: IValueWrapper<AllocationADO, ConnectionException>) {
        val message: String
        val happy: Boolean
        val allocation = wrapper.value

        if (allocation != null) {

            val dateString: String = configConstants.TOAST_DATE_FORMAT.format(allocation.until)
            val placeCaption: String = allocation.place.caption
            message = context.resources.getString(R.string.message_place_reserved, placeCaption, dateString)
            happy = true
        } else {
            var messagePostfix = ""
            if (configConstants.BUILD_DEBUG) {
                val conEx = wrapper.exception!!
                messagePostfix = "\n" + conEx.message
                SLog.d("Allocation creating error: ", conEx)
            }
            message = context.resources.getString(R.string.error_place_was_not_allocated) + messagePostfix
            happy = false
        }

        val box: NotificationBox = composeAllocationResponse(message, happy)
        notificationCreator.createNotification(context, box)
    }

    fun showLeavingNotification(infoWrapper: IValueWrapper<DriverInfoADO, SemanforException>) {
        infoWrapper.validate()

        val allocation: AllocationADO? = infoWrapper.value?.allocation
        if (allocation != null) {
            val box: NotificationBox = composeLeavingNotification(allocation)
            notificationCreator.createNotification(context, box)
        }
    }

    fun showArrivingNotification(infoWrapper: IValueWrapper<DriverInfoADO, SemanforException>) {
        val driverInfoADO: DriverInfoADO? = infoWrapper.value

        if (driverInfoADO != null) {
            val box: NotificationBox = onPositiveResponse(driverInfoADO)
            notificationCreator.createNotification(context, box)
        } else {
            showErrorNotification(infoWrapper)
        }
    }

    private fun showErrorNotification(infoWrapper: IValueWrapper<DriverInfoADO, SemanforException>) {
        val ex = infoWrapper.exception ?: throw infoWrapper.illegalStateException

        val box = composeLoadingAllocationError()
        notificationCreator.createNotification(context, box)
        return
    }

    private fun onPositiveResponse(driverInfoADO: DriverInfoADO): NotificationBox {

        when(driverInfoADO.lotState) {

            LotState.FREE -> {
                val place: PlaceADO? = driverInfoADO.freePlaces?.firstOrNull()
                place ?: throw IllegalArgumentException("DriverInfo illegal state. Lot state is free but there is no free place.")
                return composeAllocatePlace(place)
            }

            LotState.FULL -> {
                return composeLotIsFull()
            }

            LotState.RESERVATION -> {
                val allocation: AllocationADO = driverInfoADO.allocation ?: throw IllegalArgumentException("DriverInfo illegal state. Lot state is reservation but there is no allocation object.")
                val placeCaption: String = allocation.place.caption
                return composeReservation(placeCaption)
            }

            else -> throw IllegalArgumentException("Uknow LotState ${driverInfoADO.lotState}")
        }
    }

    private fun drawEmoticonSad(): Bitmap {
        return BitmapFactory.decodeResource(context.resources, R.drawable.img_sad)
    }

    private fun drawCircle(placeCaption: String, @DrawableRes circleRes: Int): Bitmap {
        val textSize: Float = context.resources.getDimension(R.dimen.notification_place_number_text_size)
        val largeIconBitmap: Bitmap = androidHelper.createBitmapFromShape(context, circleRes)
        androidHelper.printTextOnBitmapCenter(largeIconBitmap, placeCaption, textSize, Color.WHITE)

        return largeIconBitmap
    }

    ///channels
    private fun errorChannel(context: Context): BoxChannel {
        return object : BoxChannel {
            override val ledColor: Int = Color.RED
            override val channelId: String = appConstants.CHANNEL_ID_ERROR
            override val channelName: String = context.resources.getString(R.string.notif_channel_name_error)
            override val channelDescription: String = context.resources.getString(R.string.notif_channel_description_error)
        }
    }

    private fun messageChannel(context: Context): BoxChannel {
        return object : BoxChannel {
            override val ledColor: Int? = null
            override val channelId: String = appConstants.CHANNEL_ID_MESSAGE
            override val channelName: String = context.resources.getString(R.string.notif_channel_name_message)
            override val channelDescription: String = context.resources.getString(R.string.notif_channel_description_message)
        }
    }

    private fun arrivingHappyChannel(context: Context): BoxChannel {
        return object : BoxChannel {
            override val ledColor: Int = Color.GREEN
            override val channelId: String = appConstants.CHANNEL_ID_ARRIVING_HAPPY
            override val channelName: String = context.resources.getString(R.string.notif_channel_name_arriving_happy)
            override val channelDescription: String = context.resources.getString(R.string.notif_channel_description_arriving_happy)
        }
    }

    private fun leavingChannel(context: Context): BoxChannel {
        return object : BoxChannel {
            override val ledColor: Int? = null
            override val channelId: String = appConstants.CHANNEL_ID_LEAVING
            override val channelName: String = context.resources.getString(R.string.notif_channel_name_leaving)
            override val channelDescription: String = context.resources.getString(R.string.notif_channel_description_leaving)
        }
    }

    ///notification boxes
    //NTF001
    private fun composeAllocatePlace(place: PlaceADO): NotificationBox {
        val placeCaption: String = place.caption
        val placeJson: String = jsonHelper.toJson(place)

        val actionAllocate = NotificationAction(context.resources.getString(R.string.action_parked),
                AppConstants.NotificationAction.ALLOCATE,
                placeJson
        )

        val actionParkedElsewhere = NotificationAction(context.resources.getString(R.string.action_parked_elsewhere),
                AppConstants.NotificationAction.OPEN_APP
        )

        return object : NotificationBox {
            override val channel: BoxChannel = arrivingHappyChannel(context)
            override val largeIcon: Bitmap = drawCircle(placeCaption, GREEN_CIRCLE)
            override val title: String = context.resources.getString(R.string.message_park_to_place, placeCaption)
            override val notificationID: Int = appConstants.NOTIFICATION_ID_POSITIVE
            override val actions: Array<NotificationAction> = arrayOf(actionAllocate, actionParkedElsewhere)
        }
    }

    //NTF002 || NTF004
    private fun composeAllocationResponse(message: String, happy: Boolean): NotificationBox {
        val boxChannel = if (happy) messageChannel(context) else errorChannel(context)
        return object : NotificationBox {
            override val channel: BoxChannel = boxChannel
            override val largeIcon: Bitmap? = null
            override val title: String = message
            override val notificationID: Int = appConstants.NOTIFICATION_ID_ALLOCATION_RESPONSE
        }
    }

    //NTF003
    private fun composeLotIsFull(): NotificationBox {
        return object : NotificationBox {
            override val channel: BoxChannel = errorChannel(context)
            override val largeIcon: Bitmap = drawEmoticonSad()
            override val title: String = context.resources.getString(R.string.message_no_free_places)
            override val notificationID: Int = appConstants.NOTIFICATION_ID_NO_FREE_PLACES
        }
    }

    //NTF005
    private fun composeReservation(placeCaption: String): NotificationBox {
        return object : NotificationBox {
            override val channel: BoxChannel = arrivingHappyChannel(context)
            override val largeIcon: Bitmap = drawCircle(placeCaption, BLACK_CIRCLE)
            override val title: String = context.resources.getString(R.string.message_you_have_reserved_place, placeCaption)
            override val notificationID: Int = appConstants.NOTIFICATION_ID_HAS_RESERVATION
        }
    }

    //NTF006
    private fun composeLoadingAllocationError(): NotificationBox {
        return object : NotificationBox {
            override val channel: BoxChannel = errorChannel(context)
            override val largeIcon: Bitmap = drawEmoticonSad()
            override val title: String = context.resources.getString(R.string.error_loading_allocation_failed)
            override val notificationID: Int = appConstants.NOTIFICATION_ID_LOADING_ALLOCATION_ERROR
        }
    }

    //NTF007
    private fun composeLeavingNotification(allocation: AllocationADO): NotificationBox {
        val placeCaption: String = allocation.place.caption
        val json: String = jsonHelper.toJson(allocation)

        val actionRelease = NotificationAction(context.resources.getString(R.string.action_release_place),
                AppConstants.NotificationAction.DEALLOCATE,
                json
        )

        val actionKeep = NotificationAction(context.resources.getString(R.string.action_keep_place),
                AppConstants.NotificationAction.DO_NOTHING
        )

        return object : NotificationBox {
            override val channel: BoxChannel = leavingChannel(context)
            override val largeIcon: Bitmap = drawCircle(placeCaption, RED_CIRCLE)
            override val title: String = context.resources.getString(R.string.message_do_you_want_to_release_place, placeCaption)
            override val notificationID: Int = appConstants.NOTIFICATION_ID_LEAVING
            override val actions: Array<NotificationAction> = arrayOf(actionRelease, actionKeep)
        }
    }
}