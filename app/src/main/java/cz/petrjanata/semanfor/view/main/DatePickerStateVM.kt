package cz.petrjanata.semanfor.view.main

import java.util.*

class DatePickerStateVM(val visible: Boolean) {
    var selectedDate: Date = Date(0)
}