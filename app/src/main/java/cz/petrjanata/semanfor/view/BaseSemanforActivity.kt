package cz.petrjanata.semanfor.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import cz.petrjanata.semanfor.R
import kotlin.reflect.KClass

/**
 * Created by Petr Janata on 25.03.2018.
 */
abstract class BaseSemanforActivity : AppCompatActivity() {

    protected val TAG = javaClass.simpleName
    protected val activity: BaseSemanforActivity = this
    protected val context: Context = this

    protected open fun initToolbar() {
        setSupportActionBar(findViewById(R.id.semanfor_toolbar))
    }

    fun startActivity(activityClass: KClass<out Activity>): Unit {
        startActivity(activityClass.java)
    }

    fun startActivity(activityClass: Class<out Activity>): Unit {
        startActivity(Intent(this, activityClass))
    }
}