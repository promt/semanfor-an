package cz.petrjanata.semanfor.view.reservation

import android.arch.lifecycle.Observer
import android.support.v7.app.AlertDialog

open class DialogDelegate(private val activity: ReservationActivity) {

    protected open lateinit var viewModel: ReservationViewModel

    fun initViewModel(reservationViewModel: ReservationViewModel) {
        viewModel = reservationViewModel
        viewModel.getDialogMessage().observe(activity, Observer { message: String? ->
            message?.let { openMessageDialog(it) }
        })
    }

    fun showMessageDialog(message: String) {
        viewModel.setDialogMessage(message)
    }

    private fun openMessageDialog(message: String) {
        if (message.isEmpty().or(message.isBlank())) {
            return
        }

        val builder = AlertDialog.Builder(activity)
        builder.setMessage(message)
        builder.setCancelable(true)
        builder.setOnCancelListener {
            viewModel.setDialogMessage(null)
            activity.finish()
        }

        val alertDialog = builder.create()
        alertDialog.show()
    }

}