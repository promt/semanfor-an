package cz.petrjanata.semanfor.view

import android.arch.lifecycle.ViewModel

abstract class BaseSemanforViewModel : ViewModel() {

    fun knownIssue(): IllegalStateException {
        return IllegalStateException("Unknown state of live data. Wrapper should never be null. See https://stackoverflow.com/questions/50292394/why-is-the-value-param-nullable-in-observer-from-android-architecture-components")
    }

}