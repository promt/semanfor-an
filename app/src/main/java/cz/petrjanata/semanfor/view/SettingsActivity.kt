package cz.petrjanata.semanfor.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Context
import android.content.SharedPreferences
import android.location.LocationManager
import android.os.Bundle
import android.support.annotation.RequiresPermission
import android.view.MenuItem
import android.widget.CompoundButton
import cz.petrjanata.semanfor.R
import kotlinx.android.synthetic.main.activity_settings.*
import android.widget.Toast
import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.BuildConfig
import cz.petrjanata.semanfor.config.AppConstants
import cz.petrjanata.semanfor.config.ConfigConstants
import cz.petrjanata.semanfor.service.AndroidHelper
import cz.petrjanata.semanfor.service.receiver.LocationBroadcastReceiver

/**
 * Created by Petr Janata on 05.04.2018.
 */
class SettingsActivity : BaseSemanforActivity() {

    private val preferences: SharedPreferences = App.kodein.instance()
    private val configConstants: ConfigConstants = App.kodein.instance()
    private val appConstants: AppConstants = App.kodein.instance()
    private val androidHelper: AndroidHelper = App.kodein.instance()


    private var notifyOnArriving: Boolean = false
    private var notifyOnLeaving: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        initData()
        initToolbar()
        initView()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            // Respond to the action bar's Up/Home/back button
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }


    private fun initData() {
        if (!androidHelper.isLocationPermissionGranted(activity)) {
            notifyOnArriving = false
            notifyOnLeaving = false
            return
        }

        notifyOnArriving = preferences.getBoolean(appConstants.PREF_KEY_ARRIVING_NOTIFICATION_ON, false)
        notifyOnLeaving = preferences.getBoolean(appConstants.PREF_KEY_LEAVING_NOTIFICATION_ON, false)
    }

    protected override fun initToolbar() {
        super.initToolbar()
        val actionBar = this.supportActionBar
                ?: throw IllegalStateException("Actionbar is not initialized by toolbar.")
        actionBar.setDisplayHomeAsUpEnabled(true)
    }

    private fun initView() {
        arriveSwitch.isChecked = notifyOnArriving
        arriveSwitch.setOnCheckedChangeListener(this::onArriveSwitchCheckedChange)

        leaveSwitch.isChecked = notifyOnLeaving
        leaveSwitch.setOnCheckedChangeListener(this::onLeaveSwitchCheckedChange)

        appVersionTV.text = BuildConfig.VERSION_NAME
    }


    @SuppressLint("MissingPermission")
    private fun onSwitchCheckedChange(buttonView: CompoundButton, isChecked: Boolean, pendingIntent: PendingIntent) {
        if (!isChecked) {
            removePendingIntent(pendingIntent)
            return
        }

        val hasPermission = androidHelper.isLocationPermissionGranted(activity)

        if (hasPermission) {
            setupPendingIntent(pendingIntent)
        } else {
            showText(R.string.label_grant_location_permission)
            buttonView.isChecked = false
            val intent = PermissionActivity.getIntent(this, Manifest.permission.ACCESS_FINE_LOCATION)
            startActivity(intent)
        }
    }

    @RequiresPermission(anyOf = [Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION])
    @SuppressLint("MissingPermission")
    private fun setupPendingIntent(pendingIntent: PendingIntent): Unit {
        val locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.addProximityAlert(
                configConstants.GPS_PARKING_LATITUDE,
                configConstants.GPS_PARKING_LONGITUDE,
                configConstants.GPS_PARKING_RADIUS,
                -1,
                pendingIntent
        )
    }

    private fun removePendingIntent(pendingIntent: PendingIntent) {
        val locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.removeProximityAlert(pendingIntent)
    }

    private fun showText(textResource: Int) {
        showText(getString(textResource))
    }

    private fun showText(text: String) {
        Toast.makeText(activity, text, Toast.LENGTH_LONG).show()
    }

    ///callbacks
    private fun onArriveSwitchCheckedChange(buttonView: CompoundButton, isChecked: Boolean) {
        onSwitchCheckedChange(buttonView, isChecked, LocationBroadcastReceiver.getRadiusEnterPIntent(context))
        notifyOnArriving = buttonView.isChecked
        preferences.edit().putBoolean(appConstants.PREF_KEY_ARRIVING_NOTIFICATION_ON, notifyOnArriving).apply()
    }

    private fun onLeaveSwitchCheckedChange(buttonView: CompoundButton, isChecked: Boolean) {
        onSwitchCheckedChange(buttonView, isChecked, LocationBroadcastReceiver.getRadiusExitPIntent(context))
        notifyOnLeaving = buttonView.isChecked
        preferences.edit().putBoolean(appConstants.PREF_KEY_LEAVING_NOTIFICATION_ON, notifyOnLeaving).apply()
    }
}