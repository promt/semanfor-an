package cz.petrjanata.semanfor.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.R
import cz.petrjanata.semanfor.usecase.DateUsecase
import cz.petrjanata.semanfor.view.main.MainActivity

class SleepingActivity : BaseSemanforActivity() {

    private val dateUsecase: DateUsecase = App.kodein.instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sleeping)
    }

    override fun onResume() {
        super.onResume()
        checkSleepingState()
    }

    fun checkSleepingState(view: View? = null) {
        if (!dateUsecase.isAppSleeping()) {
            switchToMainActivity()
        }
    }

    private fun switchToMainActivity(): Unit {
        val intent = Intent(activity, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

}