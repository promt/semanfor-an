package cz.petrjanata.semanfor.view.signin

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.github.salomonbrys.kodein.instance
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.R
import cz.petrjanata.semanfor.service.google.SignInHelper
import cz.petrjanata.semanfor.view.BaseSemanforActivity
import cz.petrjanata.semanfor.view.main.MainActivity
import kotlinx.android.synthetic.main.activity_sign_in.*


/**
 * Created by Petr Janata on 28.03.2018.
 */
@SuppressLint("RestrictedApi")
/** Known issue https://developers.google.com/android/guides/releases#march_20_2018_-_version_1200.*/
class SignInActivity : BaseSemanforActivity() {

    private val signInHelper: SignInHelper = App.kodein.instance()

    private val RC_SIGN_IN: Int = 236 //random value

    ///inhereted instance methods
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        signInButton.setOnClickListener(this::signIn)

        checkUserAlreadySignedIn()
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            val account = signInHelper.handleSignInResult(task)
            if (account != null) {
                openApp()
            }
        }
    }

    ///private instance methods
    private fun checkUserAlreadySignedIn() {

        if (signInHelper.getServerAuthCode() != null) {
            openApp()
            finish()
            return
        }

        signInHelper.silentSignIn(this::onSilentSignIn)
    }

    private fun openApp() {
        val intent = Intent(activity, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }


    ///call backs
    private fun signIn(buttonView: View?): Unit {
        val signInIntent: Intent = signInHelper.getSignInIntent()
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun onSilentSignIn(account: GoogleSignInAccount?) {
        if (account == null) {
            //do nothing, user needs to sign in manually
            return
        }

        openApp()
    }

}