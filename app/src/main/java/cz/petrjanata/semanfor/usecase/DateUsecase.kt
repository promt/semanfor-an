package cz.petrjanata.semanfor.usecase

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.config.ConfigConstants
import java.util.*


class DateUsecase {

    private val configConstants: ConfigConstants = App.kodein.instance()

    val HOUR: Long = 60 * 60 * 1000
    val MORNING: Int = configConstants.MORNING
    val EVENING: Int = configConstants.EVENING



    fun getMorningOrNow(): Date {
        val now = Date()
        val morning: Date = getMorning(now)

        return if (now > morning) now else morning
    }

    fun getEveningOrNow(): Date {
        val now = Date()
        val evening: Date = getEvening(now)

        return if (now > evening) now else evening
    }

    fun getMorning(day: Date): Date {
        val cal = Calendar.getInstance()
        cal.time = day

        cal.set(Calendar.HOUR_OF_DAY, MORNING)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)

        return cal.time
    }

    fun getEvening(day: Date): Date {
        val cal = Calendar.getInstance()
        cal.time = day

        cal.set(Calendar.HOUR_OF_DAY, EVENING)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)

        return cal.time
    }

    fun isAppSleeping(): Boolean {
        if (!configConstants.SLEEP_AFTER_WORKING_HOURS) {
            return false
        }

        val now = Date()
        val evening = getEvening(now)
        return now > evening
    }

    fun getMinReservationDate(): Date {
        val now = Date().time
        return Date(now + configConstants.RESERVATION_PROLONGATION)
    }
}