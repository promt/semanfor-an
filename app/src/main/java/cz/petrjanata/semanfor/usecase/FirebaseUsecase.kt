package cz.petrjanata.semanfor.usecase

import com.google.firebase.iid.FirebaseInstanceId

class FirebaseUsecase {
    fun getPushToken(): String? {
        return FirebaseInstanceId.getInstance().token
    }
}