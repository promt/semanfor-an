package cz.petrjanata.semanfor.usecase

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.service.web.ApiWebService
import cz.petrjanata.semanfor.service.driverinfo.DriverInfoManager
import cz.petrjanata.semanfor.helper.executeAsync
import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.model.HttpStatus
import cz.petrjanata.semanfor.model.ResponseValidationException
import java.util.*

class RequestDriverInfoUsecase {

    private val driverInfoManager: DriverInfoManager = App.kodein.instance()
    private val apiWebService: ApiWebService = App.kodein.instance()
    private val receiveUsecase: ReceiveDriverInfoUsecase = App.kodein.instance()
    private val getServerAuthCodeUseCase: GetServerAuthCodeUseCase = App.kodein.instance()
    private val firebaseUsecase: FirebaseUsecase = App.kodein.instance()
    private val dateUsecase: DateUsecase = App.kodein.instance()

    fun requestDriverInfo(day: Date?) {

        val since: Date
        if (day==null) {
            since = dateUsecase.getMorningOrNow()
        } else {
            since = dateUsecase.getMorning(day)
        }

        val until: Date
        if (day==null) {
            until = dateUsecase.getEveningOrNow()
        } else {
            until = dateUsecase.getEvening(day)
        }

        getServerAuthCodeUseCase.getServerAuthCodeSilentlyAsync {serverAuthCode: String? ->
            executeAsync {
                sendRequestToBE(serverAuthCode, since, until)
            }
        }
    }

    fun registerListener(listener: DriverInfoManager.DriverInfoListener) {
        driverInfoManager.registerListener(listener)
    }

    fun unregisterListener(driverInfoListener: DriverInfoManager.DriverInfoListener) {
        driverInfoManager.unregisterListener(driverInfoListener)
    }

    private fun sendRequestToBE(serverAuthCode: String?, since: Date, until: Date) {
        serverAuthCode ?: throw IllegalStateException("User is signed out.")

        val pushToken: String = firebaseUsecase.getPushToken() ?: TODO()

        val response = apiWebService.requestAvailablePlaces(serverAuthCode = serverAuthCode, pushToken = pushToken, since = since, until = until)

        response.exception?.let { ex ->
            receiveUsecase.postException(ex)
            return
        }

        response.validate()
        val code = response.value!!.code
        if (code < HttpStatus.OK || code >= HttpStatus.MULTIPLE_CHOICES) {
            receiveUsecase.postException(ResponseValidationException("Unsupported response code $code when requesting available places."))
            return
        }

        if (code != HttpStatus.ACCEPTED) {
            SLog.w("Unexpected status code $code when requesting available places.")
        }
    }

}
