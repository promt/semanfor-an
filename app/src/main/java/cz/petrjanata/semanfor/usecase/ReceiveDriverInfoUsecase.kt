package cz.petrjanata.semanfor.usecase

import android.support.annotation.WorkerThread
import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.model.SemanforException
import cz.petrjanata.semanfor.api.model.DriverInfoADO
import cz.petrjanata.semanfor.model.ValueWrapper
import cz.petrjanata.semanfor.service.driverinfo.DriverInfoManager

class ReceiveDriverInfoUsecase {

    private val driverInfoManager: DriverInfoManager = App.kodein.instance()

    @WorkerThread
    fun postDriverInfo(driverInfo: DriverInfoADO) {
        driverInfoManager.getAllListeners().forEach { listener ->
            listener.onDriverInfoResponse(ValueWrapper(driverInfo))
        }
    }

    @WorkerThread
    fun postException(exception: SemanforException) {
        driverInfoManager.getAllListeners().forEach { listener ->
            listener.onDriverInfoResponse(ValueWrapper(exception))
        }
    }
}