package cz.petrjanata.semanfor.usecase

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.service.google.SignInHelper
import cz.petrjanata.semanfor.helper.log.SLog

/**
 * Created by Petr Janata on 12.08.2018.
 */
class GetServerAuthCodeUseCase {

    private val TAG = javaClass.simpleName
    private val signInHelper: SignInHelper = App.kodein.instance()

    /**
     * Pass serverAuthCode to the callback after silent sign in. Pass null when manual
     * sign in is required.
     */
    fun getServerAuthCodeSilentlyAsync(callback: (String?)->Unit): Unit {
        signInHelper.silentSignIn {
            val newServerCode = signInHelper.getServerAuthCode()
            newServerCode ?: SLog.v("ServerAuthCode is null after silent sign in. Manual sign in is required.")
            callback(newServerCode)
        }
    }
}