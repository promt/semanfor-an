package cz.petrjanata.semanfor.usecase

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.api.model.AllocationADO
import cz.petrjanata.semanfor.helper.executeAsync
import cz.petrjanata.semanfor.helper.executeUi
import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.model.ConnectionException
import cz.petrjanata.semanfor.model.HttpStatus
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.model.ResponseValidationException
import cz.petrjanata.semanfor.service.web.ApiResponse
import cz.petrjanata.semanfor.service.web.ApiWebService
import java.util.*

class ReservationUsecase {

    private val serverAuthCodeUseCase: GetServerAuthCodeUseCase = App.kodein.instance()
    private val apiWebService: ApiWebService = App.kodein.instance()
    private val dateUsecase: DateUsecase = App.kodein.instance()


    /**
     * @param day provide null for today and any other Date instance for future day
     */
    fun createAllocation(day: Date?, placeID: String, callback: (IValueWrapper<AllocationADO, ConnectionException>) -> Unit) {

        val since: Date
        if (day==null) {
            since = dateUsecase.getMorningOrNow()
        } else {
            since = dateUsecase.getMorning(day)
        }

        val until: Date
        if (day==null) {
            until = dateUsecase.getEveningOrNow()
        } else {
            until = dateUsecase.getEvening(day)
        }

        val difference = Math.abs(since.time - until.time)
        val second = 1000L
        if (difference < second) {
            SLog.w("Creating allocation with no length.")
        }

        createAllocation(since, until, placeID, callback)
    }

    fun createAllocation(since: Date, until: Date, placeID: String, callback: (IValueWrapper<AllocationADO, ConnectionException>) -> Unit) {
        serverAuthCodeUseCase.getServerAuthCodeSilentlyAsync { serverAuthCode ->
            serverAuthCode ?: throw IllegalStateException("User is signed out.")
            executeAsync {
                val wrapper: IValueWrapper<AllocationADO, ConnectionException> = apiWebService.createAllocation(serverAuthCode, since, until, placeID)
                wrapper.validate()

                executeUi { callback(wrapper) }
            }
        }
    }

    fun releaseAllocation(allocation: AllocationADO, callback: (ConnectionException?) -> Unit) {
        serverAuthCodeUseCase.getServerAuthCodeSilentlyAsync { serverAuthCode ->
            serverAuthCode ?: throw IllegalStateException("User is signed out.")
            executeAsync {
                val now = Date()
                val conEx: ConnectionException?

                if (allocation.until < now) {
                    SLog.v("${javaClass.simpleName}#releaseAllocation", "Allocation already expired. No need of cutting it.")
                    conEx = null
                } else if (allocation.since < now) {
                    conEx = cutAllocation(serverAuthCode, allocation)
                } else {
                    conEx = deleteAllocation(serverAuthCode, allocation)
                }

                executeUi { callback(conEx) }
            }
        }
    }

    private fun cutAllocation(serverAuthCode: String, allocation: AllocationADO): ConnectionException? {
        val now = Date()
        allocation.until = now

        val wrapper: IValueWrapper<AllocationADO, ConnectionException> = apiWebService.editAllocation(serverAuthCode, allocation)
        wrapper.validate()

        return wrapper.exception
    }

    private fun deleteAllocation(serverAuthCode: String, allocation: AllocationADO): ConnectionException? {
        val wrapper: IValueWrapper<ApiResponse<Void?>, ConnectionException> = apiWebService.deleteAllocation(serverAuthCode, allocation)
        wrapper.validate()

        val conEx = wrapper.exception
        if (conEx != null) {
            return conEx
        }


        val code: Int = wrapper.value!!.code

        if (code < HttpStatus.OK || code >= HttpStatus.MULTIPLE_CHOICES) {
            return ResponseValidationException("Unsupported response code $code when deleting allocation ${allocation.eventID}.")
        }

        if (code != HttpStatus.NO_CONTENT) {
            SLog.w("Unexpected status code $code when deleting allocation ${allocation.eventID}.")
        }

        return null
    }
}