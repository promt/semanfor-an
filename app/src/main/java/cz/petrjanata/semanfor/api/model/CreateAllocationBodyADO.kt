package cz.petrjanata.semanfor.api.model

import java.util.*

class CreateAllocationBodyADO() {
    lateinit var serverAuthCode: String
    lateinit var since: Date
    lateinit var until: Date
    lateinit var placeID: String

    constructor(serverAuthCode: String, since: Date, until: Date, placeID: String) : this() {
        this.serverAuthCode = serverAuthCode
        this.since = since
        this.until = until
        this.placeID = placeID
    }
}