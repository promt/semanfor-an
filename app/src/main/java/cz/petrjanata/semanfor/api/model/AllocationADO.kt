package cz.petrjanata.semanfor.api.model

import com.fasterxml.jackson.annotation.JsonFormat
import cz.petrjanata.semanfor.api.ApiConstants
import java.util.*

class AllocationADO {
    lateinit var eventID: String
    @JsonFormat(pattern= ApiConstants.JSON_DATE_FORMAT)
    lateinit var since: Date
    @JsonFormat(pattern= ApiConstants.JSON_DATE_FORMAT)
    lateinit var until: Date
    lateinit var place: PlaceADO

    //for json serialization
    constructor()

    constructor(eventID: String, since: Date, until: Date, place: PlaceADO) {
        this.eventID = eventID
        this.since = since
        this.until = until
        this.place = place
    }
}