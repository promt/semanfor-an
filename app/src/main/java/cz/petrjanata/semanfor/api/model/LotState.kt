package cz.petrjanata.semanfor.api.model

enum class LotState {
    FREE, FULL, RESERVATION
}