package cz.petrjanata.semanfor.api.model

class DriverADO {
    lateinit var driverName: String
    lateinit var email: String
    lateinit var phoneNumber: String
    lateinit var car: String
    var carRegistration: String? = null

    constructor()

    constructor(driverName: String, email: String, phoneNumber: String, car: String, carRegistration: String?) {
        this.driverName = driverName
        this.email = email
        this.phoneNumber = phoneNumber
        this.car = car
        this.carRegistration = carRegistration
    }

}