package cz.petrjanata.semanfor.service.web

import cz.petrjanata.semanfor.api.ApiConstants
import cz.petrjanata.semanfor.api.model.AllocationADO
import cz.petrjanata.semanfor.api.model.CreateAllocationBodyADO
import cz.petrjanata.semanfor.api.model.EditAllocationBodyADO
import retrofit2.Call
import retrofit2.http.*

interface ApiRetrofitProtocol {

    @GET(ApiConstants.END_POINT_PLACES)
    fun getAvailablePlaces(@Query(ApiConstants.PARAM_SERVER_AUTH_CODE) serverAuthCode: String,
                           @Query(ApiConstants.PARAM_PUSH_TOKEN) pushToken: String,
                           @Query(ApiConstants.PARAM_SINCE) sinceString: String,
                           @Query(ApiConstants.PARAM_UNTIL) untilString: String) : Call<Void>

    @POST(ApiConstants.END_POINT_ALLOCATION)
    fun createAllocation(@Body createAllocationBodyADO: CreateAllocationBodyADO) : Call<AllocationADO>

    @PATCH(ApiConstants.END_POINT_ALLOCATION)
    fun editAllocation(@Body editAllocationBodyADO: EditAllocationBodyADO): Call<AllocationADO>

    @HTTP(method = "DELETE", path = ApiConstants.END_POINT_ALLOCATION, hasBody = true)
    fun deleteAllocation(@Body editAllocationBodyADO: EditAllocationBodyADO): Call<Void>
}