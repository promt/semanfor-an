package cz.petrjanata.semanfor.service.firebase

import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService

/**
 * Created by Petr Janata on 11.04.2018.
 */
class FirebaseIDService: FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        super.onTokenRefresh()
        val token: String? = FirebaseInstanceId.getInstance().token
    }
}