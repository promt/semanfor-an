package cz.petrjanata.semanfor.service.web

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.model.ConnectionException
import cz.petrjanata.semanfor.api.ApiConstants
import cz.petrjanata.semanfor.api.model.AllocationADO
import cz.petrjanata.semanfor.api.model.CreateAllocationBodyADO
import cz.petrjanata.semanfor.api.model.EditAllocationBodyADO
import cz.petrjanata.semanfor.config.ConfigConstants
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.service.web.retrofit.OkHttpClientHelper
import cz.petrjanata.semanfor.service.web.retrofit.RetrofitConfiguration
import retrofit2.Call
import java.text.SimpleDateFormat
import java.util.*

class ApiWebService(herokuBaseUrl: String? = null) {

    private val semanforApiService: WebServiceHelper = App.kodein.instance()
    private val apiConstants: ApiConstants = App.kodein.instance()
    private val configConstants: ConfigConstants = App.kodein.instance()

    ///instance attributes
    private val baseUrl: String = herokuBaseUrl ?: configConstants.BACKEND_URL
    private val headers: Map<String, String> = mapOf(ApiConstants.CONTENT_TYPE to ApiConstants.APPLICATION_JSON)
    private val okClientProvider: OkHttpClientHelper = OkHttpClientHelper()
    private val retrofitProtocol: ApiRetrofitProtocol = RetrofitConfiguration().getApiCaller(baseUrl, headers, ApiRetrofitProtocol::class.java, okClientProvider)

    private val dateFormat: SimpleDateFormat = apiConstants.dateFormat

    fun requestAvailablePlaces(serverAuthCode: String,
                               pushToken: String,
                               since: Date,
                               until: Date): IValueWrapper<ApiResponse<Void?>, ConnectionException> {
        val sinceString = dateFormat.format(since)
        val untilString = dateFormat.format(until)
        val apiCall: Call<Void> = retrofitProtocol.getAvailablePlaces(serverAuthCode, pushToken, sinceString, untilString)
        return semanforApiService.executeApiCallWithCode(apiCall)
    }

    fun createAllocation(serverAuthCode: String, since: Date, until: Date, placeID: String) : IValueWrapper<AllocationADO, ConnectionException> {
        val body = CreateAllocationBodyADO(serverAuthCode, since, until, placeID)
        val apiCall: Call<AllocationADO> = retrofitProtocol.createAllocation(body)
        return semanforApiService.executeApiCall(apiCall)
    }

    fun editAllocation(serverAuthCode: String, allocation: AllocationADO): IValueWrapper<AllocationADO, ConnectionException> {
        val body = EditAllocationBodyADO(serverAuthCode, allocation)
        val apiCall: Call<AllocationADO> = retrofitProtocol.editAllocation(body)
        return semanforApiService.executeApiCall(apiCall)
    }

    fun deleteAllocation(serverAuthCode: String, allocation: AllocationADO): IValueWrapper<ApiResponse<Void?>, ConnectionException> {
        val body = EditAllocationBodyADO(serverAuthCode, allocation)
        val apiCall: Call<Void> = retrofitProtocol.deleteAllocation(body)
        return semanforApiService.executeApiCallWithCode(apiCall)
    }
}