package cz.petrjanata.semanfor.service.google

import android.content.Intent
import android.widget.Toast
import com.github.salomonbrys.kodein.instance
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.Scope
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.api.ApiConstants
import cz.petrjanata.semanfor.helper.log.SLog

/**
 * Created by Petr Janata on 11.04.2018.
 */
class SignInHelper {

    private val TAG = javaClass.simpleName
    private val apiConstants: ApiConstants = App.kodein.instance()
    private val appContext: App = App.kodein.instance()
    private val googleSignInClient: GoogleSignInClient by lazy { GoogleSignIn.getClient(appContext, getSignInOptions()) }


    /**
     * @return serverAuthCode
     * When code is null try @see [silentSignIn]
     */
    fun getServerAuthCode(): String? {
        val account: GoogleSignInAccount? = GoogleSignIn.getLastSignedInAccount(appContext)
        val serverAuthCode: String? = account?.serverAuthCode
        return serverAuthCode
    }

    fun getSignInIntent(): Intent {
        return googleSignInClient.getSignInIntent()
    }

    /**
     * Asynchronous operation.
     * Silently try to sign in user in Google.
     * @param onSignInCallBack call back that is invoked when process is done.
     * Provide either @see [GoogleSignInAccount] object when sing in was successful
     * or null when sign in failed.
     */
    fun silentSignIn(onSignInCallBack: (GoogleSignInAccount?) -> Unit) {
        googleSignInClient.silentSignIn()
                .addOnCompleteListener(object : OnCompleteListener<GoogleSignInAccount> {
                    override fun onComplete(task: Task<GoogleSignInAccount>) {
                        val signInAccount: GoogleSignInAccount? = handleSignInResult(task)
                        onSignInCallBack(signInAccount)
                    }
                })
    }

    fun handleSignInResult(completedTask: Task<GoogleSignInAccount>): GoogleSignInAccount? {
        try {
            val account: GoogleSignInAccount = completedTask.getResult(ApiException::class.java)
            // Signed in successfully
            return account
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            SLog.w(TAG, "signInResult:failed code=" + e.statusCode)
            if (e.statusCode == CommonStatusCodes.NETWORK_ERROR) {
                //todo use proper text resource
                Toast.makeText(appContext, "Network error.", Toast.LENGTH_LONG).show()
            } else {
//                Toast.makeText(appContext, "Error: ${e.statusCode}", Toast.LENGTH_LONG).show()
            }
            return null
        }
    }

    fun signOut(onSuccessListener: ()->Unit) {
        val onCompleteListener =  OnCompleteListener<Void> {
            onSuccessListener.invoke()
        }
        signOut(onCompleteListener)
    }

    fun signOut(onCompleteListener: OnCompleteListener<Void>): Unit {
        googleSignInClient.signOut().addOnCompleteListener(onCompleteListener)
    }


    private fun getSignInOptions(): GoogleSignInOptions {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        return GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestServerAuthCode(apiConstants.GOOGLE_WEB_CLIENT_ID)
                .requestIdToken(apiConstants.GOOGLE_WEB_CLIENT_ID)
                .requestScopes(Scope(apiConstants.GOOGLE_SCOPE_CALENDAR))
                .build()
    }

}