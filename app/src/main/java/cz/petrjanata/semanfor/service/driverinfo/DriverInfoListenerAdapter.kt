package cz.petrjanata.semanfor.service.driverinfo

import cz.petrjanata.semanfor.model.SemanforException
import cz.petrjanata.semanfor.api.model.DriverInfoADO
import cz.petrjanata.semanfor.model.IValueWrapper

//see known issue https://stackoverflow.com/questions/43469241/passing-lambda-instead-of-interface
class DriverInfoListenerAdapter : DriverInfoManager.DriverInfoListener {

    override val type: DriverInfoManager.ListenerType
    private val lambda: (IValueWrapper<DriverInfoADO, SemanforException>) -> Unit

    constructor(type: DriverInfoManager.ListenerType, lambda: (IValueWrapper<DriverInfoADO, SemanforException>) -> Unit) {
        this.type = type
        this.lambda = lambda
    }

    override fun onDriverInfoResponse(driverInfoWrapper: IValueWrapper<DriverInfoADO, SemanforException>) {
        lambda.invoke(driverInfoWrapper)
    }
}