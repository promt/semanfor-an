package cz.petrjanata.semanfor.service.web.retrofit

import cz.petrjanata.semanfor.helper.log.SLog
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor

class OkHttpClientHelper {

    private val tag = "retrofit"

    fun provide(headers: Map<String, String>?): OkHttpClient {
        return buildClient(headers)
    }

    private fun buildClient(headers: Map<String, String>?): OkHttpClient {
        val logger = okhttp3.logging.HttpLoggingInterceptor.Logger {SLog.v(tag,it)}
        val loggingInterceptor = HttpLoggingInterceptor(logger)
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        val clientBuilder = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor { chain ->
                    val request = chain.request()
                    val requestBuilder: Request.Builder = request.newBuilder()

                    requestBuilder.addHeader("Accept", "application/json")
                    if (headers != null) {
                        for ((key, value) in headers) {
                            requestBuilder.addHeader(key, value)
                            SLog.v(tag,"Header: $key = $value")
                        }
                    }
                    chain.proceed(requestBuilder.build())
                }

        return clientBuilder.build()
    }

}