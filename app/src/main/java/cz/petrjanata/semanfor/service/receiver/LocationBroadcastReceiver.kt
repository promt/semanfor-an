package cz.petrjanata.semanfor.service.receiver

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.api.model.DriverInfoADO
import cz.petrjanata.semanfor.config.AppConstants
import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.model.SemanforException
import cz.petrjanata.semanfor.service.driverinfo.DriverInfoManager
import cz.petrjanata.semanfor.usecase.RequestDriverInfoUsecase
import cz.petrjanata.semanfor.view.notification.NotificationView

/**
 * Created by Petr Janata on 02.04.2018.
 */
class LocationBroadcastReceiver : BroadcastReceiver() {

    companion object {

        private val appConstants: AppConstants = App.kodein.instance()
        private const val requestCodeKey = appConstants.RADIUS_REQUEST_CODE_EXTRA_KEY

        fun getRadiusEnterPIntent(context: Context): PendingIntent {
            val intent = Intent(context, LocationBroadcastReceiver::class.java)
            intent.putExtra(requestCodeKey, appConstants.RADIUS_ENTER_REQUEST_CODE)

            return PendingIntent.getBroadcast(context, appConstants.RADIUS_ENTER_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        fun getRadiusExitPIntent(context: Context): PendingIntent {
            val intent = Intent(context, LocationBroadcastReceiver::class.java)
            intent.putExtra(requestCodeKey, appConstants.RADIUS_EXIT_REQUEST_CODE)

            return PendingIntent.getBroadcast(context, appConstants.RADIUS_EXIT_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }
    }

    private val requestDriverInfoUsecase: RequestDriverInfoUsecase = App.kodein.instance()
    private val notificationView: NotificationView = App.kodein.instance()

    override fun onReceive(context: Context, intent: Intent) {
        //The fired Intent will have a boolean extra added with key KEY_PROXIMITY_ENTERING.
        // If the value is true, the device is entering the proximity region; if false, it is exiting.
        val entering: Boolean = intent.extras.get(LocationManager.KEY_PROXIMITY_ENTERING) as Boolean
        val requestCode: Int = intent.extras.getInt(requestCodeKey, 0)
        SLog.i("onReceive location | entering:$entering | request code: $requestCode")

        if (entering && requestCode == appConstants.RADIUS_ENTER_REQUEST_CODE) {
            onEntering()
        } else if (!entering && requestCode == appConstants.RADIUS_EXIT_REQUEST_CODE) {
            onExiting()
        }
    }

    private fun onEntering() {
        requestDriverInfoUsecase.registerListener(object : DriverInfoManager.DriverInfoListener {
            override val type: DriverInfoManager.ListenerType = DriverInfoManager.ListenerType.ARRIVING_NOTIFICATION

            override fun onDriverInfoResponse(driverInfoWrapper: IValueWrapper<DriverInfoADO, SemanforException>) {
                requestDriverInfoUsecase.unregisterListener(this)
                notificationView.showArrivingNotification(driverInfoWrapper)
            }
        })

        requestDriverInfoUsecase.requestDriverInfo(null)
    }

    private fun onExiting() {
        requestDriverInfoUsecase.registerListener(object : DriverInfoManager.DriverInfoListener {
            override val type: DriverInfoManager.ListenerType = DriverInfoManager.ListenerType.LEAVING_NOTIFICATION

            override fun onDriverInfoResponse(driverInfoWrapper: IValueWrapper<DriverInfoADO, SemanforException>) {
                requestDriverInfoUsecase.unregisterListener(this)
                notificationView.showLeavingNotification(driverInfoWrapper)
            }
        })

        requestDriverInfoUsecase.requestDriverInfo(null)
    }


}