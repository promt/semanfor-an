package cz.petrjanata.semanfor.service.driverinfo

import cz.petrjanata.semanfor.api.model.DriverInfoADO
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.model.SemanforException

class DriverInfoManager {

    private var arrivingNotificationListener: DriverInfoListener? = null
    private var leavingNotificationListener: DriverInfoListener? = null
    private var mainActivityListener: DriverInfoListener? = null
    private var reservationActivityListener: DriverInfoListener? = null


    fun getAllListeners(): Collection<DriverInfoListener> {
        leavingNotificationListener?.let { return setOf(it) }
        reservationActivityListener?.let { return setOf(it) }
        mainActivityListener?.let { return setOf(it) }
        arrivingNotificationListener?.let { return setOf(it) }

        return emptySet()
    }

    fun registerListener(listener: DriverInfoListener) {
        when(listener.type) {
            ListenerType.ARRIVING_NOTIFICATION -> arrivingNotificationListener = listener
            ListenerType.LEAVING_NOTIFICATION -> arrivingNotificationListener = listener
            ListenerType.MAIN_ACTIVITY -> mainActivityListener = listener
            ListenerType.RES_ACTIVITY -> reservationActivityListener = listener
            else -> throw IllegalArgumentException("Unknown type of DriverInfoListener: ${listener.type}")
        }
    }

    fun unregisterListener(listener: DriverInfoListener) {
        when(listener.type) {
            ListenerType.ARRIVING_NOTIFICATION -> arrivingNotificationListener = null
            ListenerType.LEAVING_NOTIFICATION -> arrivingNotificationListener = null
            ListenerType.MAIN_ACTIVITY -> mainActivityListener = null
            ListenerType.RES_ACTIVITY -> reservationActivityListener = null
            else -> throw IllegalArgumentException("Unknown type of DriverInfoListener: ${listener.type}")
        }
    }


    interface DriverInfoListener {
        val type: ListenerType
        fun onDriverInfoResponse(driverInfoWrapper: IValueWrapper<DriverInfoADO, SemanforException>)
    }

    enum class ListenerType {
        ARRIVING_NOTIFICATION,
        LEAVING_NOTIFICATION,
        MAIN_ACTIVITY,
        RES_ACTIVITY,
    }
}