package cz.petrjanata.semanfor.service.web

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.github.salomonbrys.kodein.instance
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.api.ApiConstants
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import kotlin.reflect.KClass

class JsonHelper {

    private val apiConstants: ApiConstants = App.kodein.instance()
    private val gson: Gson by lazy { buildGson() }
    private val objectMapper: ObjectMapper by lazy { buildObjectMapper() }


    @Throws(RuntimeException::class)
    fun <T: Any> fromJson(json: String, classOfT: KClass<T>): T {
        return fromJson(json, classOfT.java)
    }

    @Throws(RuntimeException::class)
    fun <T> fromJson(json: String, classOfT: Class<T>): T {
        return objectMapper.readValue<T>(json, classOfT)
    }

    fun toJson(src: Any): String {
        return objectMapper.writeValueAsString(src)
    }

    fun buildDefaultConverterFactory(): Converter.Factory {
        return getJacksonFactory()
    }



    private fun getGsonFactory(): Converter.Factory {
        return GsonConverterFactory.create(gson)
    }

    private fun getJacksonFactory(): Converter.Factory {
        return JacksonConverterFactory.create(objectMapper)
    }

    private fun buildGson(): Gson {
        return GsonBuilder()
                .setDateFormat(apiConstants.JSON_DATE_FORMAT)
                .create()
    }

    private fun buildObjectMapper(): ObjectMapper {
        val objectMapper = ObjectMapper()
        objectMapper.dateFormat = apiConstants.dateFormat
        objectMapper.registerModule(KotlinModule())
        return objectMapper
    }
}