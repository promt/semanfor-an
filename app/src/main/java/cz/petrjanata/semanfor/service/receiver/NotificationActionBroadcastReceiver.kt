package cz.petrjanata.semanfor.service.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.app.NotificationManagerCompat
import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.api.model.AllocationADO
import cz.petrjanata.semanfor.api.model.PlaceADO
import cz.petrjanata.semanfor.config.AppConstants
import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.service.web.JsonHelper
import cz.petrjanata.semanfor.usecase.ReservationUsecase
import cz.petrjanata.semanfor.view.notification.NotificationView
import cz.petrjanata.semanfor.view.signin.SignInActivity

class NotificationActionBroadcastReceiver : BroadcastReceiver() {

    private val appConstants: AppConstants = App.kodein.instance()
    private val jsonHelper: JsonHelper = App.kodein.instance()
    private val notificationView: NotificationView = App.kodein.instance()
    private val reservationUsecase: ReservationUsecase = App.kodein.instance()


    override fun onReceive(context: Context, intent: Intent) {
        val notificationID: Int = intent.getIntExtra(appConstants.NOTIFICATION_ID_EXTRA_KEY, appConstants.NOTIFICATION_ID_INVALID_VALUE)
        val actionTypeName: String = intent.getStringExtra(appConstants.NOTIFICATION_ACTION_TYPE_EXTRA_KEY) ?: throw IllegalArgumentException("Action type name is not provided.")
        val json: String? = intent.getStringExtra(actionTypeName)
        val actionType = AppConstants.NotificationAction.valueOf(actionTypeName)

        cancelNotification(context, notificationID)

        when (actionType) {
            AppConstants.NotificationAction.ALLOCATE -> allocate(json ?: throw IllegalArgumentException("Action type $actionTypeName need valid json extra."))
            AppConstants.NotificationAction.OPEN_APP -> openApp(context)
            AppConstants.NotificationAction.DO_NOTHING -> Unit
            AppConstants.NotificationAction.DEALLOCATE -> deallocate(json ?: throw IllegalArgumentException("Action type $actionTypeName need valid json extra."))
            else -> throw IllegalArgumentException("Unknown action type $actionType")
        }
    }


    private fun cancelNotification(context: Context, notificationID: Int) {
        val manager = NotificationManagerCompat.from(context)
        manager.cancel(notificationID)
    }

    private fun allocate(json: String) {
        val place: PlaceADO = jsonHelper.fromJson(json, PlaceADO::class)
        reservationUsecase.createAllocation(null, place.calendarID, notificationView::onAllocationResponse)
    }

    private fun openApp(context: Context) {
        val intent = Intent(context, SignInActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)
    }

    private fun deallocate(json: String) {
        val allocation: AllocationADO = jsonHelper.fromJson(json, AllocationADO::class)
        reservationUsecase.releaseAllocation(allocation) { connectionException ->
            if (connectionException == null) {
                SLog.d("Dealocation was successful.")
            } else {
                SLog.i("Dealocation failed.", connectionException)
            }
        }
    }
}