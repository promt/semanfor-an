package cz.petrjanata.semanfor.service

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.location.LocationManager
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.v4.app.ActivityCompat
import android.widget.TextView
import cz.petrjanata.semanfor.R
import kotlin.math.roundToInt

/**
 * Created by Petr Janata on 05.04.2018.
 */
class AndroidHelper {

    fun isLocationPermissionGranted(context: Context): Boolean {
            return (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
    }

    /**
     * Method return true if attest one of GPS_PROVIDER or NETWORK_PROVIDER are enabled.
     * Remember there is no dependency between location settings and permissions.
     */
    fun isLocationSettingsAllowed(context: Context): Boolean {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var anyProviderEnabled = false
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            anyProviderEnabled = true
        }
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            anyProviderEnabled = true
        }
        return anyProviderEnabled
    }

    fun createBitmapFromShape(context: Context, @DrawableRes shapeRes: Int ): Bitmap {
        val drawable: Drawable = context.resources.getDrawable(shapeRes)
        return drawableToBitmap(drawable)
    }

    fun drawableToBitmap(drawable: Drawable): Bitmap {

        if (drawable is BitmapDrawable) {
            return drawable.bitmap
        }

        val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)

        return bitmap
    }

    fun printTextOnBitmapCenter(image: Bitmap, text: String, textSize: Float, @ColorInt textColor: Int) {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.textSize = textSize
        paint.color = textColor
        paint.textAlign = Paint.Align.CENTER

        //somehow it works don't ask me how
        var baseline = -paint.ascent() // ascent() is negative
        val width: Int    // round
        width = (paint.measureText(text) + 0.5f).roundToInt()
        val height: Int
        height = (baseline + paint.descent() + 0.5f).roundToInt()


        baseline = (image.height - height) / 2F
        baseline = textSize + baseline

        val xCoordinate: Float = image.width / 2F

        val canvas = Canvas(image)
        canvas.drawText(text, xCoordinate, baseline, paint)
    }

}

fun TextView.setTextColorRes(@ColorRes res: Int) {
    val color = resources.getColor(res)
    setTextColor(color)
}

fun TextView.setText(@StringRes id: Int, vararg args: Any?) {
    val string: String = resources.getString(id, *args)
    text = string
}