package cz.petrjanata.semanfor.service.web

data class ApiResponse<T>(val responseDto: T?, val code: Int)