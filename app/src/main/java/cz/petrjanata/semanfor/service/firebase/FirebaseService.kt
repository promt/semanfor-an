package cz.petrjanata.semanfor.service.firebase

import android.support.annotation.WorkerThread
import com.github.salomonbrys.kodein.instance
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.api.model.DriverInfoADO
import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.service.web.JsonHelper
import cz.petrjanata.semanfor.usecase.ReceiveDriverInfoUsecase

/**
 * Created by Petr Janata on 11.04.2018.
 */
class FirebaseService : FirebaseMessagingService() {

    private val receiveDriverInfoUsecase: ReceiveDriverInfoUsecase = App.kodein.instance()
    private val jsonHelper: JsonHelper = App.kodein.instance()

    @WorkerThread
    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        SLog.v(message.data.toString())
        val json: String? = message.data["driverInfo"]

        if (json == null) {
            SLog.w("Received driverInfo is null.")
            return
        }

        val driverInfo: DriverInfoADO
        driverInfo = jsonHelper.fromJson(json, DriverInfoADO::class.java)
        receiveDriverInfoUsecase.postDriverInfo(driverInfo)
    }
}