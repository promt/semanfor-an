package cz.petrjanata.semanfor.service.web

import cz.petrjanata.semanfor.model.ConnectionException
import cz.petrjanata.semanfor.model.UnsupportedResponseException
import cz.petrjanata.semanfor.model.IValueWrapper
import cz.petrjanata.semanfor.model.ValueWrapper
import cz.petrjanata.semanfor.helper.log.SLog
import cz.petrjanata.semanfor.model.HttpStatus.ACCEPTED
import cz.petrjanata.semanfor.model.HttpStatus.CREATED
import cz.petrjanata.semanfor.model.HttpStatus.NOT_MODIFIED
import cz.petrjanata.semanfor.model.HttpStatus.NO_CONTENT
import cz.petrjanata.semanfor.model.HttpStatus.OK
import retrofit2.Call
import retrofit2.Response
import java.io.IOException

class WebServiceHelper {

    ///methods for delegates
    internal fun <T > executeApiCall(
            apiCall: Call<T>): IValueWrapper<T, ConnectionException> {

        val responseValueWrapper = executeApiCallWithCode(apiCall)

        val code = responseValueWrapper.value?.code ?: return ValueWrapper(responseValueWrapper.exception)
        val responseDTO = responseValueWrapper.value?.responseDto
        if (responseDTO is List<*>) {
            val message = "Do not use this method for lists. Use overloaded variant with "
            val ex = UnsupportedOperationException(message)
            SLog.e(message, ex)
            return ValueWrapper(ConnectionException(ex))
        } else {
            return ValueWrapper(responseDTO)
        }
    }


    internal fun <T> executeApiCallWithCode(apiCall: Call<T>): IValueWrapper<ApiResponse<T?>, ConnectionException> {

        val response: Response<T>

        try {
            response = apiCall.execute()
        } catch (ex: IOException) {
            return ValueWrapper(ConnectionException(ex))
        }

        if (response.isSuccessful) {
            return onResponse(response)
        } else {
            val message = "${response.code()}: ${response.message()}\n${response.errorBody()?.string()}"
            return ValueWrapper(ConnectionException(message))
        }

    }

    ///private methods
    private fun <T> onResponse(response: Response<T>?): IValueWrapper<ApiResponse<T?>, ConnectionException> {
        when (response?.code()) {
            OK, CREATED -> {
                val body: T? = response.body()
                val apiResponse: ApiResponse<T?> = ApiResponse(body, response.code())
                return ValueWrapper(apiResponse)
            }

            ACCEPTED, NO_CONTENT, NOT_MODIFIED -> {
                val apiResponse: ApiResponse<T?> = ApiResponse(null, response.code())
                return ValueWrapper(apiResponse, null)
            }

            else -> {
                val ex = UnsupportedResponseException("" + response?.errorBody()?.source())
                return ValueWrapper(ex)
            }
        }
    }

}