package cz.petrjanata.semanfor.service.web.retrofit

import com.github.salomonbrys.kodein.instance
import cz.petrjanata.semanfor.App
import cz.petrjanata.semanfor.service.web.JsonHelper
import okhttp3.OkHttpClient
import retrofit2.Retrofit

class RetrofitConfiguration {

    private val jsonHelper: JsonHelper = App.kodein.instance()

    fun <T> getApiCaller(baseUrl: String,
                         headers: Map<String, String>?,
                         retrofitServiceClass: Class<T>,
                         okHttpClientProvider: OkHttpClientHelper = OkHttpClientHelper()): T {

        val client: OkHttpClient = okHttpClientProvider.provide(headers)

        val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(jsonHelper.buildDefaultConverterFactory())
                .build()

        val retrofitProtocol: T = retrofit.create(retrofitServiceClass)
        return retrofitProtocol
    }

}