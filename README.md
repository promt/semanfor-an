# SeManfor
Project SeManfor is part of diploma thesis made on University of Economics in Prague by Petr Janata. 
The thesis solves problem of limited number of parking spaces, that are located 
in front of JABLOTRON OFFICE PARK in Prague Holešovice, dedicated to eMan company. 
SeManfor increase automatization in park places reservation process. 
By that SeManfor increase efficiency of usage the park places.
SeManfor system is dedicated for employees of eMan s.r.o.

Project SeManfor is combination of mobile application and server backend. 
The mobile application is for Android operation system and it is written in the Kotlin programming language. 
Backend application is made as REST API solution. 
It is based on Spring framework, written in Kotlin and runs on Heroku platform.

[Android app repository](https://gitlab.com/promt/semanfor-an.git)
[backend app repository](https://gitlab.com/promt/semanfor-be.git)

 
## Android app
This repository contains source codes of Android mobile app of project SeManfor.
Source codes of this program are published under [MIT licence](./LICENSE)
Some sensitive data were removed from the source files in order of its publishing. 
For example: the google-services.json file or signing key files etc.
